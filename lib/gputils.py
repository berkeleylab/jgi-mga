#!/usr/bin/env python

import sys
import getpass
import subprocess
import argparse
import re
import collections
import os as _os

def get_time(string):
    if not re.match("[0-9]{1,3}:[0-5][0-9]:[0-5][0-9]",string):
        raise argparse.ArgumentTypeError("%s is not a valid time format" % string)
    return tuple(string.split(":")) 

def get_callback(string):
    host,port = None,None
    try:
        host,port = string.split(":")
        port = int(port)
    except Exception:
        raise argparse.ArgumentTypeError("%s is not a valid time format" % string)
    #return tuple(string.split(":")) 
    return (host,int(port)) 

def get_UGE_arguments(parser,def_job_name,mem=2,time="08:00:00", pe_slots=8):
    group = parser.add_argument_group("UGE Options", "options for submitting to genepool")
    group.add_argument("-s", "--share", dest="share", type=str, default="gentech-rqc.p", help="the project share to run under when submitting to genepool [gentech-rqc.p]")
    group.add_argument("-m", "--mem", dest="memory", type=float, metavar="MEM", default=mem, help="the amount of memory per slot, in Gb, to request when submitting to genepool. ["+str(mem)+"]")
    group.add_argument("-t", "--time", dest="run_time", type=get_time, metavar="HH:MM:SS", default=time, help="the run time to request when submitting to genepool. ["+time+"]")
    group.add_argument("-p", "--pe_slots", dest="threads", type=int, metavar="SLOTS", default=pe_slots, help="the number of slots to request when submitting to genepool. ["+str(pe_slots)+"]")
    group.add_argument("--hyperthread", dest="hyperthread", action="store_true", help="run all programs with 2 threads/slot")
    group.add_argument("-N", "--name", dest="name", type=str, metavar="NAME", default=def_job_name, help="the name to submit to genepool under ["+def_job_name+"]")
    group.add_argument("-e", "--email", dest="email", type=str, metavar="NAME", default=getpass.getuser()+"@nersc.gov", help="the address to send email to when job is complete ["+getpass.getuser()+"@nersc.gov]")
    group.add_argument("--no_email", dest="no_email", action="store_true", default=False, help="do not send email when job is complete")
    group.add_argument("--wait", dest="wait", action="store_true", default=False, help="wait for genepool job to complete")
    group.add_argument("--hold", metavar="JOBID", type=int, help="Genepool job id to hold for when submitting this job")
    group.add_argument("--tmpscratch", dest="tmpscratch", action="store_true", help="run in temporary local scratch")
    group.add_argument("--callback", type=get_callback, default=None, metavar="HOST:PORT", help="host and port to call back to when job completes")
#end get_UGE_arguments

def write_UGE_arguments (ostream, args, log_file=None):
    ostream.write("#$ -w e\n")
    if isinstance(args,dict):
        if 'email' in args: 
            ostream.write("#$ -m ase\n")
            ostream.write("#$ -M "+args['email']+"\n")
        ostream.write("#$ -cwd \n")
        ostream.write("#$ -P "+args['share']+"\n")
        ostream.write("#$ -j yes\n")
        ostream.write("#$ -l h_rt=%s:%s:%s\n" % args['run_time'])
        ostream.write("#$ -l ram.c="+str(args['memory'])+"G\n")
        ostream.write("#$ -pe pe_slots "+str(args['threads'])+"\n")
            
    else:
        if not args.no_email:
            ostream.write("#$ -m ase\n")
            ostream.write("#$ -M "+args.email+"\n")
        ostream.write("#$ -cwd \n")
        ostream.write("#$ -P "+args.share+"\n")
        ostream.write("#$ -j yes\n")
        ostream.write("#$ -l h_rt=%s:%s:%s\n" % args.run_time)
        ostream.write("#$ -l ram.c="+str(args.memory)+"G\n")
        ostream.write("#$ -pe pe_slots "+str(args.threads)+"\n")
    if log_file:
        ostream.write("#$ -o %s\n" % log_file)
    ostream.write("\n")
    ostream.write("set -o pipefail\n")
    
#end write_UGE_arguments

def add_trap_statement(ostream, condition="SIGTERM", addl_fail_cmds=None):
    if addl_fail_cmds:
        ostream.write("trap \"%s; exit 100\" %s\n" % (addl_fail_cmds.replace("\"","\\\""),condition))
    else:
        ostream.write("trap \"exit 100\" %s\n" % condition)

def write_command (ostream, cmd, callback_server=None):
    ostream.write("cmd=\"%s\"\n" % cmd)
    ostream.write("eval $cmd\n")
    ostream.write("status=$?\n")
    ostream.write("if [ $status != 0 ]; then\n")
    ostream.write("    echo -e \"The following command exited with status $status:\\n $cmd\"\n")
    if callback_server:
        ostream.write("    nc -c \"echo $JOB_ID:100\" %s %s\n" % callback_server)
    ostream.write("    exit 100\n")
        
    ostream.write("fi\n")
#end write_command

def submit_to_genepool(shell_script, job_name, hold=None):
    cmd = [ "qsub", "-N", job_name, shell_script ]
    if hold:
        cmd.insert(1, "-hold_jid")
        cmd.insert(2, str(hold))
    sys.stdout.write(" ".join(cmd)+"\n")
    subprocess.Popen(cmd).wait()
#end submit_to_genepool

def run_locally (sh, log_file=None):
    cmd=[ "bash", sh ]
    return subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=log_file)

def write_dated_echo(ostream,statement):
    ostream.write("echo \"[`date +'%%Y-%%m-%%d %%H:%%M:%%S'`] %s\"\n" % statement)

class Command:
    def __init__(self,command, log_msg=None):
        self.command = command.replace('`','\`')
        self.log_msg = log_msg

    def get_command(self):
        return self.command
    
    def has_log_msg(self):
        return not self.log_msg is None
    
    def get_log_msg(self):
        return self.log_msg


class JobCompletionEvaluator:
    def __init__(self):
        self.commands = list()
        self.outputs = list()

    def add_command(self, command, outputs):
        self.outputs.append(outputs)
        self.commands.append(command)
    
    def get_commands(self):
        def __file_exists__(path):
            return not (_os.path.isfile(path) and _os.path.getsize(path) > 0)
        incomplete_commands = list()
        for i in xrange(len(self.outputs)):
            if not all(map(__file_exists__,self.outputs[i])):
                incomplete_commands.append(self.commands[i])
        return tuple(incomplete_commands)
        
class GPShellScriptBuilder:
    
    def __init__ (self, gp_args, restart=False):
        self.commands = list()
        self.env_variables = collections.OrderedDict()
        self.outdir = None
        self.gp_args = gp_args
        self.modules = set()
        self.trap_actions = list()
        self.restart = restart
        if isinstance(gp_args,dict):
            if 'callback' in gp_args:
                self.callback_server = (gp_args['callback'][0],str(gp_args['callback'][1]))
            else:
                self.callback_server = None
        else:
            if gp_args.callback:
                self.callback_server = (gp_args.callback[0],str(gp_args.callback[1]))
            else:
                self.callback_server = None
            
        self.req_outputs = list()
    
    def add_log_file(self, log_file):
        self.log_file = log_file
    
    def add_command(self, command, log_msg=None):
        if isinstance(command, Command):
            self.commands.append(command)
        else:
            self.commands.append(Command(command,log_msg))

    
    def add_env_var(self, var, val):
        self.env_variables[var] = val
    
    def add_outdir(self,outdir,tmpscratch=False):
        self.outdir = outdir
        self.tmpscratch = tmpscratch
        
    def add_modules(self,*module):
        for mod in module:
            self.modules.add(mod)
    
    def add_trap_action(self, action):
        self.trap_actions.append(action)
    
    def add_required_output(self,req_output):
        self.req_outputs.append(req_output)

    def set_callback_server(self,host,port):
        self.callback_server = (host,str(port))

    def write_script(self, ostream):

        ostream.write("#!/bin/bash\n")
        ostream.write("#\n")
        write_UGE_arguments(ostream, self.gp_args, self.log_file)
        ostream.write("\n")
        
        # load modules
        ostream.write("#begin: load modules\n")
        for module in self.modules:
            ostream.write("module load %s\n" % module)
        ostream.write("#end:   load modules\n")
        ostream.write("\n")
        
        # set any environment variables
        for var,val in self.env_variables.iteritems():
            ostream.write("export %s=\"%s\"\n" % (var,val))
        ostream.write("\n")
        
        # set output directory
        if self.tmpscratch:
            ostream.write("export OUTDIR=\"/tmp/$JOB_NAME.$JOB_ID\"\n")
        else:
            ostream.write("export OUTDIR=\"%s\"\n" % self.outdir)
        ostream.write("if [ ! -d $OUTDIR ]; then\n\tmkdir $OUTDIR\nfi\n")
        ostream.write("\n")
        
        if self.tmpscratch and self.restart:
            ostream.write("rsync -r %s $OUTDIR/\n" % self.outdir)

        # add trap statements
        actions = list() 
        if self.tmpscratch:
            actions.append("rsync -r $OUTDIR/ %s" % self.outdir)
        for action in self.trap_actions:
            actions.append(action)
        if self.callback_server:
            actions.append("nc -c \"echo $JOB_ID:SIGTERM\" %s %s" % self.callback_server)

        if len(actions) > 0:
            add_trap_statement(ostream, condition="SIGTERM", addl_fail_cmds="; ".join(actions))
            
            if self.callback_server:
                actions.pop()
                actions.append("nc -c \"echo $JOB_ID:SIGKILL\" %s %s" % self.callback_server)
            
            add_trap_statement(ostream, condition="SIGKILL", addl_fail_cmds="; ".join(actions))
            #echo !!!! Run time or memory limit exceeded !!!!; 
            ostream.write("\n")

        write_dated_echo(ostream, "Working on $HOSTNAME and writing output to $OUTDIR")
        ostream.write("cd $OUTDIR\n")
        ostream.write("\n")
        
        # write commands
        for command in self.commands:
            if command.has_log_msg():
                write_dated_echo(ostream, "%s" % command.get_log_msg())
            ostream.write("cmd=\"%s\"\n" % command.get_command())
            ostream.write("eval $cmd\n")
            ostream.write("status=$?\n")
            ostream.write("if [ $status != 0 ]; then\n")
            ostream.write("    echo -e \"The following command exited with status $status:\\n $cmd\"\n")
            if self.tmpscratch:
                ostream.write("    rsync -r $OUTDIR $OLDPWD\n")
                ostream.write("    rm -r $OUTDIR\n")
            if self.callback_server:
                ostream.write("    nc -c \"echo $JOB_ID:100\" %s %s\n" % self.callback_server)
            ostream.write("    exit 100\n")
            ostream.write("fi\n")
            ostream.write("\n")
    
        ostream.write("cd -\n")
        ostream.write("\n")

        # move temp dir if one was used
        if self.tmpscratch:
            write_dated_echo(ostream, "Moving temp dir $OUTDIR to %s" % self.outdir)
            ostream.write("if [ ! -d %s ]; then\n\tmkdir %s\nfi\n" % (self.outdir, self.outdir))
            ostream.write("rsync -r $OUTDIR/ %s\n" % self.outdir)
            ostream.write("rm -r $OUTDIR\n")
            ostream.write("chmod 755 %s\n" % self.outdir)
            ostream.write("chgrp genome %s\n" % self.outdir)
            ostream.write("\n")
        
        
        for req_output in self.req_outputs: 
            ostream.write("if [ ! -s %s/%s ]; then\n" % (self.outdir,req_output))
            ostream.write("    echo \"**** Required output file %s not found in %s/\"\n" % (req_output,self.outdir))
            if self.callback_server:
                ostream.write("    nc -c \"echo $JOB_ID:99\" %s %s\n" % self.callback_server)
            ostream.write("    exit 100\n")
            ostream.write("fi\n")
            
        
        # add callback for completion
        if self.callback_server:
            ostream.write("nc -c \"echo $JOB_ID:0\" %s %s\n" % self.callback_server)

class GenepoolJob:
    def __init__(self):
        self.job_evaluator = JobCompletionEvaluator()
        self.modules = set() 
        self.genepool_args = list()
        self.log_files = list()
        self.shell_scripts = list()
        self.env_variables = list()
        self.outdir = "."

    def get_last_genepool_arguments(self):
        return self.genepool_args[:-1]
    
    def write_script(self, gp_args, shell_script, log_file, restart=False):
        # store data on all jobs that have been run
        self.log_files.append(log_file)
        self.shell_scripts.append(shell_script)
        self.genepool_args.append(gp_args)

        writer = GPShellScriptBuilder(gp_args)

        writer.add_modules(*self.modules)

        writer.add_log_file(log_file)
        
        for (env_var, value) in self.env_variables:
            writer.add_env_var(env_var,value)

        writer.add_outdir(self.outdir,gp_args['tmpscratch'])

        for (cmd,log_msg) in self.job_evaluator.get_commands():
            writer.add_command(cmd,log_msg)

        sh = open(shell_script,"w")
        writer.write_script(sh)
        sh.close()
        return shell_script
    
