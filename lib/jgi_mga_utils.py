#!/usr/bin/env python
'''
jat_mga_utils.py
has jgi_mga utility routines
'''

import urllib2
import os
import json
import sys
import collections
import subprocess
import time
import glob
import re

def web_services(service, data, **kwargs):
    '''
    Given service type and data, return appropriate webservice call results
    args:
       service: [jamo, ap_byspid, ap, rqc_by_su]
       data: data structure for query example {metadata.sequencing_project_id: 1111111}
    '''
    method = 'GET'
    if service.lower() == "jamo" or service == None:
        full_url = 'https://sdm2.jgi-psf.org/api/metadata/query'
        method = 'POST'
    elif service.lower() == "ap_byspid": #get method
        full_url = 'http://proposals.jgi-psf.org/pmo_webservices/analysis_projects?sequencing_project_id=' + str(data)
        method = 'GET'
    elif service.lower() == "ap":
        if isinstance(data, basestring):
            full_url = 'https://proposals.jgi-psf.org/pmo_webservices/analysis_project/' + data
#            return json.loads(os.popen('curl --silent --fail --insecure ' + full_url).read())
        elif isinstance(data, (int, long, float, complex)):
            full_url = 'https://proposals.jgi-psf.org/pmo_webservices/analysis_project/' + str(data)
        elif isinstance(data, dict) and "analysis_project_id" in data.keys():
            full_url = 'https://proposals.jgi-psf.org/pmo_webservices/analysis_project/' + str(data['analysis_project_id'])
        else:
            sys.exit("data type of data: " + str(type(data)) + " not recognized")
    elif service.lower() == 'rqc_by_su':
        if isinstance(data, basestring):
            full_url = 'https://rqc.jgi-psf.org/api/readqc/report/' + data
        elif isinstance(data, dict) and "seq_unit_name" in data.keys():
            full_url = 'https://rqc.jgi-psf.org/api/readqc/report/' + str(data['seq_unit_name'])
        else:
            sys.exit("data type of data: " + str(type(data)) + " not recognized")
    elif service.lower() == 'rqc_dump_by_su':
        if isinstance(data, basestring):
            full_url = 'https://rqc.jgi-psf.org/api/rqcws/dump/' + data
#            return json.loads(os.popen('curl --silent --fail --insecure ' + full_url).read())
        elif isinstance(data, dict) and "seq_unit_name" in data.keys() and len(data['seq_unit_name']==1):
            full_url = 'https://rqc.jgi-psf.org/api/rqcws/dump/' + str(data['seq_unit_name'][0])
        else:
            sys.exit("data type of data: " + str(type(data)) + " not recognized")

    else:
        sys.exit("service type not recognized: " + str(service) + " use jamo,ap_spid,ap or rqc_by_su")

    if method == 'GET' and isinstance(data, dict):
        full_url += '?'
        for key in data:
            full_url += '%s=%s&'%(key, data[key])

    req = urllib2.Request(full_url)
    req.get_method = lambda: method
    data = json.dumps(data)
    req.add_header('Content-type', 'application/json')
    if method == 'POST':
        req.add_data(data)
        req.add_header('Content-length', len(data))

    request = urllib2.urlopen(req)
    try:
        response = request.read()
    except:
        sys.exit("Error: " + full_url + " " + data)
    response = json.loads(response)
    return response

def run_sub(cmd, fileprefix, leave=False):
    '''
    Given command, runs subprocess
    args: cmd:command to be executed
          fileprefix:prefix for command file and output
          leave: True = dont cleanup commands
    '''
    with open(fileprefix + ".cmd", "w") as writefile:
        writefile.write(cmd)

    sub_p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    ## Talk with date command i.e. read data from stdout and stderr. Store this info in tuple ##
    (output, err) = sub_p.communicate()
    ## Wait for date to terminate. Get return returncode ##
    p_status = sub_p.wait()
    if p_status == 0:
        with open(fileprefix + ".o", "w") as writefile:
            writefile.write(output)
        with open(fileprefix + ".e", "w") as writefile:
            writefile.write(err)
    else:
        sys.stdout.write("Subprocess failed for " + cmd)
        sys.stdout.write("STDOUT\n" + output + "\n\n")
        sys.stdout.write("STDERR\n" + err + "\n\n")
    if not leave:
        os.remove(fileprefix + ".cmd")
        os.remove(fileprefix + ".o")
        os.remove(fileprefix + ".e")

def gettimestamp():
    '''Returns time stamp of the format. 2017-06-14 12:10:59'''
    return time.strftime("%Y-%m-%d %H:%M:%S")

def get_glob(glob_txt, latest=False):
    '''
    Given a glob string, return file
    args: glob string, and flag to return the newest match
    returns: filenam or None if not found
    '''
    globs = glob.glob(glob_txt)
    globs.sort(key=os.path.getmtime)
    if len(globs) > 1 and latest:
        return globs[-1]
    elif len(globs) > 1:
        sys.stdout.write("Multiple " + glob_txt)
        return None
    elif len(globs) < 1:
        sys.stdout.write("No " + glob_txt)
        return None
    else:
        return globs[0]

def hasher():
    '''supports perl style auto-vivification of arbitry depth dicts'''
    return collections.defaultdict(hasher)

def sdm_query_fastq_normal(id_):
    '''
    Given a library id, return jamo record
    args: id
    returns: jamo record
    '''
    string_id = str(id_)
    if re.match(r'^[A-Z]{4,5}$', string_id): #lib id
        result = web_services('jamo', {'metadata.library_name':string_id, 'metadata.fastq_type':'sdm_normal'})
    elif re.match(r'^\d{7,7}$', string_id): #spid
        result = web_services('jamo', {'metadata.sequencing_project_id':int(string_id), 'metadata.fastq_type':'sdm_normal'})
    else:
        sys.exit(id + " type not recognized as library or spid")
    return result

