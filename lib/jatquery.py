#!/usr/bin/env python

import os,sys,json
 
#curl = Curl('https://sdm-dev.jgi-psf.org:8034')
#response = curl.post('api/analysis/query',data={"options.release_to":"img","omics.submission_id":{"$exists":False}} )
import EnvironmentModules as EnvMod
EnvMod.module(['load','jamo/prod'])
for x in os.getenv("PYTHONPATH").split(":"):
    if x not in sys.path:
        sys.path.append(x)
from sdm_curl import Curl


def get_submission_keys(seqprojid):
    curl = Curl('https://sdm2.jgi-psf.org')  
    releaseanalyses = curl.post('api/analysis/query',data={"metadata.sequencing_project_id":seqprojid})
    return releaseanalyses
    
    
