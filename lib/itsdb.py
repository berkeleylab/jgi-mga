#!/usr/bin/env python
'''  
  my $DB               = "dwprd1";
   my $NA_shared        = "dw_user";
   my $PW_shared        = "dw_user";

   $ENV{ORACLE_HOME}    = "/jgi/tools/oracle_client/DEFAULT";
   $ENV{TNS_ADMIN}      = "/jgi/tools/oracle_client/DEFAULT/network/admin";
   $ENV{TWO_TASK}       = $DB;


   my $dbh_shared = "";
   unless( $dbh_shared = DBI->connect( "dbi:Oracle:$DB","$NA_shared","$PW_shared" ) ) {
   print "cannot establish database connection to $DB\n";
        exit;
        }
return $dbh_shared;
'''

import os
import sys

def get_data(sql):    
    try:
      import cx_Oracle
    except Exception, e:
      print "Couldn't load Oracledb: " +  e.message 
      sys.exit()
    
    
    
    conn_str = u'dw_user/dw_user@dwprd1'
    conn = cx_Oracle.connect(conn_str)
    c = conn.cursor()
    c.execute(sql)
    if c:
        return c
    conn.close()
    
