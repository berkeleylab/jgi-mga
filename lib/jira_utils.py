#!/usr/bin/env python

import sys
import re
import getpass
import os.path

from jira.client import JIRA

def get_JIRA():
    user = getpass.getuser()
    pw = None
    try: 
        with open("%s/.jira_pass" % os.path.expanduser("~"), "r") as pw_file:
            pw = pw_file.readline()[:-1]
    except IOError:
        raise Exception("Could not find password file in home directory. Please add your JIRA pass to ~/.jira_pass")
    jira = JIRA(options={'server': 'https://issues.jgi-psf.org'},basic_auth=(user,pw))
    return jira
#end get_JIRA

def query_jira(query,jira=get_JIRA()):
    return jira.search_issues(query,maxResults=500)
    
def get_open_tickets(jira, *library_names):
    query = 'description ~ library_names:%s AND reporter = "Jamo" AND status != "Completed"'
    def get_ticket(lib):
        issues = query_jira(query,jira)
        if len(issues) == 1:
            return issues.pop()
        else:
            return None
    ret = [ get_ticket(lib) for lib in library_names ]
    return ret[0] if len(library_names) == 1 else ret
#end get_JIRA_ticket

def get_all_tickets(jira, *library_names):
    query = 'description ~ library_names:%s AND reporter = "Jamo"'
    def get_ticket(lib):
        issues = query_jira(query,jira)
        if len(issues) == 1:
            return issues.pop()
        else:
            return None
    ret = [ get_ticket(lib) for lib in library_names ]
    return ret[0] if len(library_names) == 1 else ret
#end get_JIRA_ticket

library_name_re = re.compile("library_name:(?P<library_name>[A-Z]{4,5})")
miseq_library_name_re = re.compile("library_name:(?P<library_name>M[0-9]{4,5})")
def get_library_name(issue):
    for line in issue.fields.description.split("\n"):
        m = library_name_re.match(line)
        if m:
            return m.group('library_name')
        m = miseq_library_name_re.match(line)
        if m:
            return m.group('library_name')
    return None
#end get_library

file_name_re1 = re.compile("sequnit=(?P<file_name>[0-9]+\.[0-9]{1}\.[0-9]+\.[ATGC]{5,6}\.fastq.gz)")
file_name_re2 = re.compile("report/(?P<file_name>[0-9]+\.[0-9]{1}\.[0-9]+\.[ATGC]{5,6}\.fastq.gz)")
def get_fastq_file_name(issue):
    for line in issue.fields.description.split("\n"):
        m = file_name_re1.search(line)
        if m:
            file_name = m.group('file_name')
            return file_name
        m = file_name_re2.search(line)
        if m:
            file_name = m.group('file_name')
            return file_name
    return None
#end get_fastq_file_name

spid_re = re.compile("sequencing_project_id:(?P<sequencing_project_id>[0-9]{7})")
def get_sequencing_project_id(issue):
    for line in issue.fields.description.split("\n"):
        m = spid_re.match(line)
        if m:
            return int(m.group('sequencing_project_id'))
    return None
#end get_sequencing_project_id

def get_sequencing_project_name(issue):
    return issue.fields.summary.encode('ascii','replace').split(' - ')[1]    
#end get_sequencing_project_name

oid_re = re.compile("taxon[ _]oid:.*?(?P<taxon_oid>[0-9]+)")
def get_reference_oids(issue):
    ret = set()
    for line in issue.fields.description.split("\n"):
        m = oid_re.match(line)
        if m:
            ret.add(m.group('taxon_oid'))
    
    for comment in issue.fields.comment.comments:
        for line in comment.body.split("\n"):
            m = oid_re.match(line)
            if m:
                ret.add(m.group('taxon_oid'))  

    return ret
#end get_reference_oids

def get_JIRA_username(query, jira=get_JIRA(), project='SEQQC'):
    result = jira.search_assignable_users_for_issues(query,project=project)
    if len(result) == 0:
        return None
    elif len(result) == 1:
        return str(result[0].name)
    else:
        return tuple(str(x.name) for x in result)
#end get_JIRA_user_name
