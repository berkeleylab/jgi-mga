#!/usr/bin/env python

import argparse
import sys
import os
import subprocess
from datetime import datetime
import json
import collections
import re

import EnvironmentModules as EnvMod
from gdata.contacts.data import Region
EnvMod.module(['load','qaqc'])
EnvMod.module(['load','jamo'])
for x in (os.getenv("PYTHONPATH") if os.getenv("PYTHONPATH") else "").split(":"):
    if x not in sys.path:
        sys.path.append(x)
import qcutils
from sdm_curl import Curl
import jgidb

version = "0.0.1"

#
# pipeline driver 
#  AP for task driven, sequence input for standard, product name?? 
#
# for products metaT,metaTcomb,metaG,metaGcomb,metaGimp.corrected,metaGimp.assembly
# starting with metaGimp.pbio_corr
#

#
# pipeline pre-process
# Input: product_type {list provided}, input data, runner template, run_folder
# Output:
# pipe.json,pipe.config,pipe.config.bash, pipe.post_process.bash
# 

#
# Pipeline post-process 
# Input: ap, run_folder, prefix(optional default see below), task_prefix(optional default see below)
# output: unless spec all
#

###
### pre = {ap}.{project_type} or specified ###
### task = pre.{at}.{product_type}
### 
# 1) {pre}.run_folder_info.json gathered info from ap, run_folder
# 2) {pre}.rqc-stats.txt
# 3) {pre}.rqc-files.txt
# 9) {pre}.log
# 9) {pre}.manifest

# 10) {task}.README.txt
# 11) {task}.README.pdf
# 12) {task}.jat.metadata.json
# 13) {task}.autoqc.analysis.txt <- autoqc analysis file
# 14) {task}.autoqc.analysis.qctest.bash <- generates auto-qc results
# 15) {task}.autoqc.analysis.qctest.jat_submit.bash <- contains test for "already jat submitted" then copies and submits
# 16) {task}.pmo_state_update.bash <- contains test for "state already set" then changes state


#
# plan: start with: metaGimp.pbio_corr SEQQC-5654
# work on gather routine
# walk through list testing as you go
# 

#
# list of all data needed for {}.run_folder_info.json (start with pbio_corr)
#
#project info its
#ap/at info pmo web service
#pmo state data
#run_folder metadata, files  and stats:
#cluster info




#seqqc for later
# 4) {pre}.{sequnit}.autoqc.readqc.txt <-autoqc analysis files
# 6) {pre}.autoqc.qctest.bash <- generates auto-qc results
# 7) {pre}.autoqc.submit.txt  <- auto_rework.py file
# 8) {pre}.autoqc.submit.bash <- contains autoqc test and "already qc'd" test and submits















def main():
    parser = argparse.ArgumentParser(description='Metagenome rqc pipeline.',usage="%(prog)s [options]")

    parser.add_argument('-v','--version', action='version',version="%(prog)s (version " +  version + ")")
    # Input: ap, run_folder, prefix(optional default see below), task_prefix(optional default see below)
    parser.add_argument("-o","--output",required=False,help="comma separated list of files to output(preproc,rqc-stats,metadata.json) default all\n")
    parser.add_argument("-a","--apid",required=True,help="metagenome analysia project id.\n")
    parser.add_argument("-f","--folder",required=True,help="analysis run_folder\n")
    parser.add_argument("-p","--prefix",required=False,help="prefix (optional default = ap\n")

    parser.add_argument("-t","--task_prefix",required=False,help="task prefix (optional default = {ap.at}\n")
    parser.add_argument("-d","--debug",default=False,action='store_true',help="[Optional] Setup but do not run\n")
    
    args = parser.parse_args()

    
    outputs = ["preprocess","rqc-stats","metadata.json"]
    if (not args.output):
        args.output = ",".join(outputs)
    for out in args.output.split(","):
        if out not in outputs:
            print out + " not in list " + ",".join
            sys.exit()

    if (not args.prefix):
        args.prefix = args.apid

    if not os.path.exists(args.folder):
        sys.exit(args.folder + " not found")

    bindir = os.path.abspath(os.path.dirname(sys.argv[0]))

    #step 1 gather mega json
    info = get_run_folder_info(args.folder,args.apid)
    
def get_run_folder_info(dir,ap):
    apinfo = get_proposal_query_AP(ap)
    apinfo = add_library_info(apinfo)
    #output file section
    
    #readme text section
    
    #print json.dumps(apinfo,indent=3)
    #get metadata.json prototype with target files
    
    #get 
    sys.exit()

def file_to_filtered(file,pick=False):
    file = os.path.basename(file)

    #get library from file metadata
    curl = Curl('https://sdm2.jgi-psf.org')
    result = curl.post('api/metadata/query',data={'file_name':file})
    if len(result)==1:
        library = query_dict(".metadata.library_name",result[0])
    else:
        sys.exit("something wrong " + str(len(result)) + " results for file_name " + file )

    #get filtered file with same library id
    result = curl.post('api/metadata/query',data={'metadata.library_name':library,'metadata.fastq_type':'filtered'})
    if len(result)==1:
        filename = query_dict("file_path",result[0]) + "/" + query_dict("file_name",result[0])
    else:
        rqcfilter_processed_paths = []
        basename_raw = os.path.splitext(os.path.splitext(file)[0])[0]
        basename_filtered = ""
        for r in result:
            if " ".join(query_dict(".metadata.filter_commands",r).find('rqcfilter.sh')) !=-1:
                filtered = r["file_path"] + "/" + r["file_name"]
                basename_filtered = ".".join(r["file_name"].split(".")[0:-3])
                if basename_raw == basename_filtered:
                    rqcfilter_processed_paths.append(filtered)
        if len(rqcfilter_processed_paths)==1 and os.path.exists(rqcfilter_processed_paths[0]):
            filename=rqcfilter_processed_paths[0]
        elif len(rqcfilter_processed_paths)==1:
            sys.stderr.write("try jamo fetch all filename " + rqcfilter_processed_paths[0] )
            sys.exit()
        elif (pick==True):
            cmd = "/bin/ls -t " + " ".join(rqcfilter_processed_paths)
#            print cmd
            filename = os.popen(cmd).readlines()[0].rstrip()
        else:
            sys.exit("something wrong " + str(len(result)) + " results for: jamo info all custom metadata.library_name=" + library + " and metadata.fastq_type=filtered also filter_commands has rqcfilter.sh" )

    return filename

def get_usable(file):
    usable = ""
    try_srf = False
    sequnit = file.split('/')[-1]
    sql = "SELECT s.rqc_state FROM DW.all_inclusive_report s WHERE s.rqc_seq_unit_name = '%s' "
    cursor = jgidb.queryDb("ITS", sql % sequnit)
    all = cursor.fetchall()
    if(len(all) >0):
        usable = str(all[0][0])
    else:
        try_srf=True

    if (try_srf):
        sequnit = sequnit.replace(".fastq.gz",".srf")
        cursor = jgidb.queryDb("ITS", sql % sequnit)
        all = cursor.fetchall()
        if(len(all) ==1):
            usable = all[0][0]
        else:
            usable = "NA"

    return usable



def add_library_info(apinfo=None,pick=None):
    if apinfo==None:
        apinfo = hasher()
    if pick==None:
        pick=False
    
    for i in range(0,len(apinfo["sequencing_projects"])):
        sdm_raw = sdm_query_fastq(apinfo["sequencing_projects"][i]["sequencing_project_id"],filtered=False)
        sdm_filt = sdm_query_fastq(apinfo["sequencing_projects"][i]["sequencing_project_id"],filtered=True)
        #print json.dumps(sdm_raw,indent=4);sys.exit()
        apinfo["sequencing_projects"][i]["lib"] = []
        for file in sdm_raw:
            lib = {}
            lib["lib"] = query_dict(".metadata.library_name",file)
            lib["spid"] = query_dict(".metadata.sequencing_project_id",file)
            lib["file"] = query_dict("file_path",file) + "/" + query_dict("file_name",file)
            lib["file"].rstrip()
            lib["file_read_count"]=query_dict(".metadata.rqc.read_qc.read_count", file)
            lib["sequnit"] = query_dict("file_name",file)

            lib["sequencer_model"] = query_dict(".metadata.sow_segment.sequencer_model",file)
            lib["platform"] = query_dict(".metadata.sow_segment.platform",file)
            lib["library_protocol"] = query_dict(".metadata.sow_segment.library_protocol",file)
            lib["target_fragment_size_bp"] = query_dict(".metadata.sow_segment.target_fragment_size_bp",file)

            lib["proposal_id"] = query_dict(".metadata.proposal.id",file)
            lib["proposal_name"] = query_dict(".metadata.proposal.title",file)
            import pdb;pdb.set_trace()
            for filt_file in sdm_filt:
                lib["filter"] = query_dict("file_path",filt_file) + "/" + query_dict("file_name",filt_file)
                lib["filter_reads_count"]=query_dict(".metadata.filter_reads_count",filt_file)
                print      query_dict(".metadata.filtered_reads_count",filt_file)       

            usable = get_usable(lib["file"])
            if (usable =="usable"):
                lib["usable"] = True
            elif(usable=="bad"):
                lib["usable"] = False
            else:
                lib["usable"] = None

            apinfo["sequencing_projects"][i]["lib"].append(lib)


    return apinfo


def get_mg_projects_and_tasks():
    ''' Return a dict of products and tasks applicable to mg'''
    hash=hasher()
    hash["10"]["product"]="Metagenome Metatranscriptome"
    hash["10"]["task"]["48"]="RNA Assembly"
    hash["38"]["product"]="Metagenome Metatranscriptome Expression - Self"
    hash["38"]["task"]["57"]="Mapping to Self"
    hash["39"]["product"]="Metagenome Metatranscriptome Expression - Other"
    hash["39"]["task"]["44"]="Mapping to Reference"
    hash["61"]["product"] = "Eukaryote Community Metatranscriptome"
    hash["61"]["task"]["48"]="RNA Assembly"
    hash["62"]["product"]="Eukaryote Community Metatranscriptome Expression - Self"
    hash["62"]["task"]="Mapping to Self"
    
    hash["7"]["product"]="Metagenome Standard Draft"
    hash["7"]["task"]["47"]="DNA Assembly"
    hash["58"]["product"]="Metagenome Minimal Draft"
    hash["58"]["task"]["47"]="DNA Assembly"
    hash["32"]["product"]="Custom Analysis"
    hash["32"]["task"]["33"]="Custom"
    hash["8"]["product"]="Metagenome Improved Draft"
    hash["8"]["task"]["47"]="DNA Assembly"
    hash["72"]["product"]="Metagenome Draft"
    hash["72"]["task"]["47"]="DNA Assembly"
    hash["73"]["product"]="PacBio Error Correction"
    hash["73"]["task"]["67"]="PacBio Error Correction"
#    print json.dumps(hash,indent=3)
    return hash

def get_proposal_query_AP(AP_id):
    '''get sp proposal info from webservice filtering for products and tasks
    found in'''
    ids = get_mg_projects_and_tasks()
    product_list = ids.keys()
    url = 'https://proposals.jgi-psf.org/pmo_webservices/analysis_project/'
#    print url + str(AP_id)
    curl = Curl(url)
    ap = curl.get(str(AP_id))
    result = hasher()
    result["analysis_project_id"] = query_dict("uss_analysis_project.analysis_project_id",ap)
    result["analysis_project_name"] = query_dict("uss_analysis_project.analysis_project_name",ap)
    result["analysis_product_type_name"] = query_dict("uss_analysis_project.analysis_product_type_name",ap)
    result["analysis_product_id"] = query_dict("uss_analysis_project.analysis_product_id",ap)
    result["sequencing_projects"] = query_dict("uss_analysis_project.sequencing_projects",ap)
    result["analysis_task_type_id"]=ids[str(result["analysis_product_id"])]["task"].keys()[0]
    result["analysis_task_type_name"]=ids[str(result["analysis_product_id"])]["task"][result["analysis_task_type_id"]]
    for j in range(0,len(ap["uss_analysis_project"]["analysis_tasks"])):
        if str(ap["uss_analysis_project"]["analysis_tasks"][j]["analysis_task_type_id"])== str(result["analysis_task_type_id"]):
            result["analysis_task_id"]=ap["uss_analysis_project"]["analysis_tasks"][j]["analysis_task_id"]
            result["analysis_task_status"]=ap["uss_analysis_project"]["analysis_tasks"][j]["current_status_id"]
    return result

def sdm_query_fastq(id,filtered=False):
    curl = Curl('https://sdm2.jgi-psf.org')
    if filtered==False:
        data_lib={'metadata.library_name':id,'metadata.fastq_type':'sdm_normal'}
        data_spid={'metadata.sequencing_project_id':int(id),'metadata.fastq_type':'sdm_normal'}
    else:
        data_lib={'metadata.library_name':id,'metadata.fastq_type':'filtered','metadata.filtered_type':'COMPRESSED FILTERED FASTQ'}
        data_spid={'metadata.sequencing_project_id':int(id),'metadata.fastq_type':'filtered','metadata.filtered_type':'COMPRESSED FILTERED FASTQ'}
    stringid = str(id)
    if (re.match('^[A-Z]{4,4}$',stringid)): #lib id
        result = curl.post('api/metadata/query',data=data_lib)
    elif(re.match('^\d{7,7}$',stringid)): #spid
        result = curl.post('api/metadata/query',data=data_spid)
    else:
        sys.exit(id + " type not recognized as library or spid")
    return result


#def sdm_query_fastq_filtered(id):# TODO :
#    curl = Curl('https://sdm2.jgi-psf.org')
#    stringid = str(id)
#    if (re.match('^[A-Z]{4,4}$',stringid)): #lib id
#        result = curl.post('api/metadata/query',data={'metadata.fastq_type':'filtered' and 'metadata.filtered_type':'COMPRESSED FILTERED FASTQ'})
#        elif(re.match('^\d{7,7}$',stringid)): #spid
#        result = curl.post('api/metadata/query',data={'metadata.fastq_type':'filtered' and 'metadata.filtered_type':'COMPRESSED FILTERED FASTQ'})
#    else:
#        sys.exit(id + " type not recognized as library or spid")
#    return result


def remnants():
    #######################create config file from info file
    createPipelineExe = bindir + "/jgi_mgaCreatePipelineJSON.py"
    cmd = [createPipelineExe, "-i", infojsonfile]
    if args.template:
        cmd.append("-t")
        cmd.append(args.template)

    run_sub(" ".join(cmd),args.output_path + "/jgi_mgaCreatePipelineJSON.py",args.debug)

    #create bash file
    configFile = infojsonfile+ ".config"
    bashFile = infojsonfile+ ".config.bash"
    fo = open(bashFile,"w")
    fo.write("#!/usr/bin/env bash\n\n")
    fo.write("cd " + args.output_path + "\n\n")
    fo.write(bindir + "/pipelineRunner.py -f "  + configFile + " 1>> " + configFile + ".o 2>| " + configFile + ".e\n")
    fo.close()

    logfile = configFile + ".o"

    #run pipeline
    if (args.debug==False):
        fo = open(logfile,"a")
        time = "Start, " + gettimestamp() + "\n" 
        fo.write(time)
        fo.close()
        qcutils.analysis_task_status(task_id,6);#"In Progress"
        run_sub("bash " + bashFile,bashFile,args.debug)
    else:
        sys.exit()

    #check pipeline output for errors  ####### verify this
    #get last line of file
    with open(logfile) as myfile:
        lastline = (list(myfile)[-1])

    if lastline.find("success")!=-1:
        pass
    elif lastline.find("error")!=-1:
        fo = open(logfile,"a")
        time = "failed, " + gettimestamp() + "\n" 
        fo.write(time)
        fo.close()
        sys.exit(1)    
    else:
        print "somthing wrong with " + logfile
        sys.exit(1)


    fo = open(logfile,"a")
    time = "complete, " + gettimestamp() + "\n"
    fo.write(time)
    fo.close()
    qcutils.analysis_task_status(task_id,8);#"Output Created and Awaiting Review"
        
def gettimestamp():
    now = datetime.now()
    return str(now.year) + "-" + str(now.month).zfill(2) + "-" + str(now.day).zfill(2) + " " + str(now.hour).zfill(2) + ":" + str(now.minute).zfill(2) + ":" + str(now.second).zfill(2)

def run_sub(cmd,fileprefix,leave=False):
    with open(fileprefix + ".cmd", "w") as file:
        file.write(cmd)
        file.close()
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    ## Talk with date command i.e. read data from stdout and stderr. Store this info in tuple ##
    (output, err) = p.communicate()
    ## Wait for date to terminate. Get return returncode ##
    p_status = p.wait()
    if (p_status == 0):
        with open(fileprefix + ".o", "w") as file:
            file.write(output)
            file.close()
        with open(fileprefix + ".e", "w") as file:
            file.write(err)
            file.close()
    else:
        sys.stdout.write("Subprocess failed for " + cmd)
        sys.stdout.write("STDOUT\n" + output + "\n\n")
        sys.stdout.write("STDERR\n" + err + "\n\n")
    if not leave:
        os.remove(fileprefix + ".cmd")
        os.remove(fileprefix + ".o")
        os.remove(fileprefix + ".e")

def hasher():
  return collections.defaultdict(hasher)

def query_dict(query,hash):
  return_value = ""
  if query.startswith("."):
    query = query[1:len(query)]
  if "." in query:      #still some levels
    fields = query.split(".")
    top_field=fields.pop(0)
    try:
      return_value = query_dict(".".join(fields),hash[top_field])
    except:
      return_value = "__NA__"
  else:
    if hash[query]!=hasher():
      return_value = hash[query]
    else:
      return_value = "__NA__"
  return return_value

if __name__ == "__main__":
    main()

