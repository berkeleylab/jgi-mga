#!/usr/bin/env python

import sys
import os

import EnvironmentModules as EnvMod
EnvMod.module(['load','qaqc'])
for x in (os.getenv("PYTHONPATH") if os.getenv("PYTHONPATH") else "").split(":"):
    if x not in sys.path:
        sys.path.append(x)
import jgidb


def main():
    arg = sys.argv[1]
    if arg.startswith("3300"):
        print get_spid_from_taxon_oid(int(arg))
    else:
        print get_taxon_oid(int(arg))



def get_taxon_oid(spid):
    '''
    Get the IMG Taxon OID for the genome in IMG with the given sequencing project id
    '''
    sql = "SELECT t.taxon_oid FROM taxon t WHERE t.jgi_project_id = %d" % spid
    taxon_oid = jgidb.queryDb("IMG",sql).next()[0]
    return taxon_oid
#end get_taxon_oid

def get_spid_from_taxon_oid(txoid):
    '''
    Get the spid for IMG Taxon OID
    '''
    sql = "SELECT t.jgi_project_id  FROM taxon t WHERE t.taxon_oid = %d" % txoid
    spid = jgidb.queryDb("IMG",sql).next()[0]
    return spid
#end get_spid_from_taxon_oid


if __name__ == "__main__":
    main()
