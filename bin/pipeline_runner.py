#!/usr/bin/env python
'''
pipeline_runner.py
takes a yaml config file and runs each subtask with dependencies

'''

import collections
import re
import os
import json
import yaml
import argparse
import time
import datetime
import sys
import subprocess
from datetime import datetime

#Todo
# checkpoint_continue key removes *.error and retries
# --restart [clean,backup]
# implement cmd list option
# stdard out testing
# add end=true var
# add stdout,stderr special vars
# add  -d dotfile option
# check everything for % symbol only allowed in cmd
# giant pipeline yaml

UNBUFFERED = os.fdopen(sys.stdout.fileno(), 'w', 0)
sys.stdout = UNBUFFERED

VERSION = "1.0.0"

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Run Pipeline.')
    parser.add_argument("config")
    parser.add_argument("-f", "--force", help="force creation of new json file even if one exists.", action="store_true")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    args = parser.parse_args()

    #test if json pipeline file already exists
    json_file = args.config + ".json"

    #create or load pipeline object
    if os.path.exists(json_file) and not args.force:
        print "reusing json_file " + json_file + ", " + get_timestamp()
        pipeline_obj = load_pipeline_from_json_file(json_file)
    elif os.path.exists(json_file) and args.force:
        print "backing up json_file" + json_file + ", " + get_timestamp()
        bu_json_file = json_file + "." + str(time.time()) + ".json"
        os.rename(json_file, bu_json_file)
        pipeline_obj = create_pipeline_from_yaml(args.config)
        print_pipeline_to_json_file(pipeline_obj, json_file)
    else:
        pipeline_obj = create_pipeline_from_yaml(args.config)
        print_pipeline_to_json_file(pipeline_obj, json_file)

    #loop through, running and updating
    counter = 0
    print "pipeline Started, " + get_timestamp()
    while not os.path.exists("__STOP__"):
        time.sleep(10)
        counter = counter + 1
        if counter > (6*60*480): # 20 day limit
            print "pipeline hit 20 day limit, " + get_timestamp()
            break
        elif pipeline_all_done(pipeline_obj):
            print "pipeline Completed successfully, " + get_timestamp()
            break
        elif pipeline_has_error(pipeline_obj):
            print "pipeline has error see " + json_file + ", " + get_timestamp()
            break
        else:
            update_status_dependencies_launch(pipeline_obj)
            print_pipeline_to_json_file(pipeline_obj, json_file)
#        print counter

    #print out final pipeline json copy
    print_pipeline_to_json_file(pipeline_obj, json_file)


def update_status_dependencies_launch(pipeline_object):
    '''
    given a pipeline object, loop through all tasks and update or launch
    args: pipeline object
    output: pipline object with modified status
    '''
    #loop through each task and update status if task is complete
    for task, value in sorted(pipeline_object.items()):
        if pipeline_object[task]["status"] in ["done", "error"]:
            continue
        if "resources" in pipeline_object[task].keys() and pipeline_object[task]["resources"].startswith("slurm"):
            if "processid" in pipeline_object[task].keys():
                cmd = 'sacct -j ' + pipeline_object[task]["processid"]  + ' --noheader --parsable | cut -f 6 -d "|" |  sort -u'
                retvalue = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read().rstrip()
                 #          slurm PENDING|RUNNING|FAILED,  or PENDING|RUNNING|COMPLETED
                if retvalue not in ["PENDING", "RUNNING"]:
                    if retvalue == "COMPLETED" and os.path.exists(pipeline_object[task]["WD"] + "/command.bash.done"):
                        print task + " finished " +  pipeline_object[task]["WD"] + ", " + get_timestamp()
                        pipeline_object[task]["status"] = "done"
                        pipeline_object[task]["timeFinish"] = str(datetime.now())
                    elif retvalue == "FAILED" and os.path.exists(pipeline_object[task]["WD"] + "/command.bash.done"):
                        pipeline_object[task]["status"] = "error_cluster"
                        print task + " cluster terminated abnormally" + ", " + get_timestamp()
                        pipeline_object[task]["timeFinish"] = str(datetime.now())
                    elif  os.path.exists(pipeline_object[task]["WD"] + "/command.bash.error"):
                        pipeline_object[task]["status"] = "error"
                        pipeline_object[task]["timeFinish"] = str(datetime.now())
                    else:
                        pipeline_object[task]["status"] = "unknown"
                else:
                    pass #still running or pending

#         if ("resources" in pipeline_object[task].keys() and pipeline_object[task]["resources"].find("drmaa") != -1): #cluster job
#             if "processid" in pipeline_object[task].keys():
#                 isjobcompletecmd = "isjobcomplete " + pipeline_object[task]["processid"] + " > /dev/null"
#                 retvalue = os.system(isjobcompletecmd) #isjobcomplete returns 0 if job is complete
#                 if retvalue  ==  0 and  os.path.exists(pipeline_object[task]["WD"] + "/command.bash.error"):
#                     pipeline_object[task]["status"] = "error"
#                     pipeline_object[task]["timeFinish"] = str(datetime.now())
#                 elif retvalue  ==  0 and os.path.exists(pipeline_object[task]["WD"] + "/command.bash.done"):
#                     print task + " finished " +  pipeline_object[task]["WD"] + ", " + get_timestamp()
#                     pipeline_object[task]["status"] = "done"
#                     pipeline_object[task]["timeFinish"] = str(datetime.now())
#                 elif retvalue  ==  0:                  #cluster booted job before completion


            if os.path.exists(pipeline_object[task]["WD"] + "/command.bash.error"): #no cluster id assigned ie fresh config
                pipeline_object[task]["status"] = "error"
                pipeline_object[task]["timeFinish"] = "previously in error state " + str(datetime.now())

            elif os.path.exists(pipeline_object[task]["WD"] + "/command.bash.done"):
                pipeline_object[task]["status"] = "done"
                pipeline_object[task]["timeFinish"] = "previously in done state " + str(datetime.now())
            else:
                pass #will launch below

        elif os.path.exists(pipeline_object[task]["WD"] + "/command.bash.error"):#for local jobs
            pipeline_object[task]["status"] = "error"
            pipeline_object[task]["timeFinish"] = str(datetime.now())

        elif os.path.exists(pipeline_object[task]["WD"] + "/command.bash.done"):
            print task + " finished " +  pipeline_object[task]["WD"] + ", " + get_timestamp()
            pipeline_object[task]["status"] = "done"
            pipeline_object[task]["timeFinish"] = str(datetime.now())

        else:
            #still running locally
            pass

    #loop through each task and update dependencies
    for task, value in sorted(pipeline_object.items()):
        if pipeline_object[task]["status"] in ["done", "error"]:
            continue

        #determine whether dependencies remain
        dependencies_remain = False
        if pipeline_object[task]["depends"]: #has dependencies
            for parent_task in pipeline_object[task]["depends"]:
                if pipeline_object[parent_task]["status"] != "done":
                    dependencies_remain = True

        if not dependencies_remain and pipeline_object[task]["status"] != "running":
            pipeline_object[task] = launch_task(pipeline_object[task], task)
            pipeline_object[task]["status"] = "running"
            pipeline_object[task]["timeStart"] = str(datetime.now())

    return pipeline_object


def write_bash(directive_list=[], cmd_list=[], cd_dir="", file_path=""):
    '''write_bash
    inputs: directive list: for cluster
            cmd_list: list of commands
            cd_dir: run folder
            file_path: where to put the file
    returns:
            file_path
    '''
    with open(file_path, "w") as writefile:
        writefile.write('#!/usr/bin/env bash' + "\n\n")
        writefile.write("ulimit -c 0\numask 002\nset -eo pipefail\n\n")
        writefile.write("cd " + os.path.realpath(cd_dir) + "\n\n")

        if len(directive_list)>0:
            for directive in directive_list:
                writefile.write(directive + "\n")
            writefile.write("\n")

        if len(cmd_list)>0:
            for cmd in cmd_list:
                writefile.write(cmd + "\n")
            writefile.write("\n")

    os.chmod(file_path, 0775)
    return file_path


def launch_task(task_obj, taskname):
    '''
    Given a taks object and task name, launch it
    args: task object, taskname
    output: task object modified with launch metadata
    '''
    cmd = []
    bash_file = task_obj["WD"] + "/command.bash"
    if "shifterimg" in task_obj.keys():
        shifter_file = task_obj["WD"] + "/shifter.bash"
        cmd.append("shifterimg pull " + task_obj['shifterimg'])
        cmd.append("shifter --image=" + task_obj['shifterimg'] + " " + bash_file)
        write_bash(cmd_list=cmd,cd_dir=task_obj["WD"],file_path=shifter_file)

    cmd = []
    cmd = [c.rstrip().lstrip() for c in task_obj["fullcmd"].split(";")]
    if task_obj["fullcmd"] == "":
        cmd.append("touch " + bash_file + ".done")
    write_bash(cmd_list=cmd,cd_dir=task_obj["WD"],file_path=bash_file)

    cmd = ""
    if "shifterimg" in task_obj.keys():
        cmd = shifter_file
    else:
        cmd = bash_file
    cmd +=  " 1>| command.bash.o 2>| command.bash.e && touch command.bash.done || touch command.bash.error\n"
    
    # write run file
    clustertaskname = "_" + os.path.basename(os.path.dirname(task_obj["WD"])) + "_" + taskname
    run_file = task_obj["WD"] + "/run.bash"
    if "resources" in task_obj.keys() and (task_obj["resources"].find("uge") != -1 or task_obj["resources"].find("slurm") != -1):
        directive_list = []
        if task_obj["resources"].startswith('uge:'):
            directive_list.append("#$ -wd " + task_obj["WD"])
            directive_list.append("#$ -N "  + clustertaskname)
            directive_list.append("#$ "  + task_obj["resources"].lstrip('uge:'))

        elif task_obj["resources"].startswith('slurm'):
#           directive_list += "#SBATCH -t 120 --mincpus=16 --mem=64G -C haswell --qos=jgi -A fungalp\n\n"
            directive_list.append("#SBATCH -J " + clustertaskname)

            if task_obj["resources"].startswith('slurmbb:'):
                directive_list.append("#SBATCH -D " + task_obj["WD"])
                directive_list.append("#SBATCH " + task_obj["resources"].lstrip('slurmbb:'))
                directive_list.append("#DW jobdw capacity=300GB access_mode=striped type=scratch")
                directive_list.append("echo $DW_JOB_STRIPED\n")

            elif task_obj["resources"].startswith('slurm:'):
                directive_list.append("#SBATCH -D " + task_obj["WD"])
                directive_list.append("#SBATCH " + task_obj["resources"].lstrip('slurm:'))

        write_bash(cmd_list=[cmd],directive_list=directive_list,cd_dir=task_obj["WD"],file_path=run_file)
        jobid = run_cluster(run_file, clustertaskname, task_obj["resources"], task_obj["WD"])
        task_obj["processid"] = jobid
        print taskname + " start processid " + jobid + ", " + get_timestamp()
    else:
        write_bash(cmd_list=[cmd],cd_dir=task_obj["WD"],file_path=run_file)
        process = subprocess.Popen(run_file, shell=True)
        jobid = process.pid
        task_obj["processid"] = jobid
        print taskname + " start local, " + get_timestamp()
    return task_obj



def run_cluster(runfile, task, clusterstring, working_dir):
    '''
    Given runfile, and taskname
    run cluster job
    returns: job id
    '''
    jobid = ""
    cmds = ''
#   cmd = "qsub " + clusterstring + " -wd " +  working_dir + " -N " + clustertaskname + " \"" + bashfile + "\""

    if clusterstring.startswith('uge'):
        qsub_cmd = "module load uge;qsub "
        if os.path.exists(working_dir + "/mem.txt"):
            cmd = 'cat ' + working_dir + '/mem.txt'
            mem = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()[0].rstrip()
            mem = float(mem) * 1.1
            if mem > 1000:
                qsub_cmd += " -l ram.c=2000G,h_rt=240:00:00 "
            elif mem > 500:
                qsub_cmd += " -l ram.c=1000G,h_rt=240:00:00 "
            elif mem > 120:
                qsub_cmd += " -l ram.c=500G,h_rt=240:00:00 "
            else:
                qsub_cmd += ""

        jobid = subprocess.Popen(qsub_cmd + runfile, shell=True, stdout=subprocess.PIPE).stdout.readlines()[0].split(" ")[2]
        
    elif clusterstring.startswith('slurm'):
        cmd = 'sbatch ' + runfile
        jobid = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()[0].split(" ")[-1].rstrip()
    else:
        sys.exit("cluster instrutions error" + clusterstring)

    return jobid


def print_pipeline_to_json_file(pipeline_object, filename):
    '''write out pipeline'''
    with open(filename, "w") as writefile:
        writefile.write(json.dumps(pipeline_object, indent=3))
    return False

def load_pipeline_from_json_file(filename):
    '''read in pipeline'''
    with open(filename, "r") as readfile:
        pipeline_object = json.loads(readfile.read())
    return pipeline_object

def create_pipeline_from_yaml(config_file):
    '''
    Given yaml config file, create pipeline structure with depencies and full paths
    args: config_file
    output: pipeline object
    '''
    pipeline = hasher()
    with open(config_file, 'r') as stream:
        try:
            pipeline = yaml.load(stream)
        except yaml.YAMLError as exc:
            print exc

    #get default working directory if not set
    cwd = os.getcwd() + '/'

    #get working directories for all tasks
    for task, value in pipeline.items():
        if "WD" not in pipeline[task].keys():
            pipeline[task]["WD"] = cwd + task
            ##dependancies can be explicitly set.  If not empty dep list added.
            if "depends" not in pipeline[task].keys():
                pipeline[task]["depends"] = []

    #double check explicitly set dependencies
    for task, value in pipeline.items():
        for dep in pipeline[task]["depends"]:
            if dep not in pipeline.keys():
                sys.exit(task + " has dependency on " + dep + " but that does not exist. See "  + config_file)

    #expand command string and add dependencies
    for task, value in pipeline.items():
        if "cmd" not in pipeline[task].keys(): #null cmd case is allowed.  task does nothing but store metadata
            pipeline[task]["cmd"] = ""

        #get all %% symbols from command string
        raw_command = pipeline[task]["cmd"]
        regex = re.compile('%.*?%')
        symbols = regex.findall(raw_command)

        #Loop through all found %% symbols and create lookup dict for later expansion
        symbol2file = dict()
        pipeline[task]["status"] = ""
        for symbol in symbols:
            original_symbol = symbol
            realpath = ""
            symbol = symbol.replace("%", "")

            if symbol.find("->") != -1: # file from another task
                taskname, var = symbol.split("->")
                if taskname not in pipeline.keys():
                    sys.exit(task + " has dependency on " + taskname + " but that does not exist. See "  + config_file)

                if var not in pipeline[taskname].keys():
                    sys.exit(var + " does not exist in " + taskname + ". See "  + config_file)

                ###pipelineRunner adds absolute path to all variables except if it starts with "/" or "__"
                if pipeline[taskname][var].startswith("/") or var.startswith("__"):
                    realpath = pipeline[taskname][var]
                else:
                    realpath = pipeline[taskname]["WD"] + "/" +  pipeline[taskname][var]

                #collect dependencies and set status
                pipeline[task]["depends"].append(taskname)
                pipeline[task]["depends"] = sorted(set(pipeline[task]["depends"])) # uniqify the list of dependencies
                pipeline[task]["status"] = "dependent"

            elif symbol.startswith("__") or pipeline[task][symbol].startswith("/"):
                ###pipelineRunner adds absolute path to all variables except if it starts with "/" or "__"
                realpath = pipeline[task][symbol]

            else: # file from same task
                if symbol not in pipeline[task].keys():
                    sys.exit(symbol + " does not exist in task " + task + ". See "  + config_file)

                else:
                    realpath = pipeline[task]["WD"] + "/" +  pipeline[task][symbol]

            symbol2file[original_symbol] = realpath


        #copy cmd string for first instance
        if "fullcmd" not in pipeline[task].keys():
            pipeline[task]["fullcmd"] = pipeline[task]["cmd"]

        #do symbol expansion
        for symbol, realpath in symbol2file.items():
            pipeline[task]["fullcmd"] = pipeline[task]["fullcmd"].replace(symbol, realpath)

        #setup directories
        if not os.path.exists(pipeline[task]["WD"]):
            os.makedirs(pipeline[task]["WD"])

    return pipeline

def get_timestamp():
    '''return timestamp in format "%Y-%m-%d %H:%M:%S"'''
    return time.strftime("%Y-%m-%d %H:%M:%S")

def pipeline_all_done(pipeline_object):
    '''return True if all tasks have status "done"'''
    done = True
    for task, value in pipeline_object.items():
        if pipeline_object[task]["status"] != "done":
            done = False
    return done

def pipeline_has_error(pipeline_object):
    '''return True if at least one task has status "error"'''
    error = False
    for task, value in pipeline_object.items():
        if pipeline_object[task]["status"] == "error":
            error = True
    return error

def hasher():
    '''auto fill arbitrary depth dict'''
    return collections.defaultdict(hasher)

if __name__ == "__main__":
    main()

