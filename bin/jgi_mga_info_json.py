#!/usr/bin/env python

import sys
import os
import argparse
import re
import collections
import json
import string
import subprocess

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__))+'/../lib')
import jgi_mga_utils

#from sdm_curl import Curl
#import jgidb

VERSION = "1.0.0"

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Metagenome rqc pipeline info file creator.',
                                     usage="%(prog)s <-o outputfile> [-s sequencing project id] [-c flag combined]" +
                                     "[-a AP id overrides -s] [-f comma separated fastq overrides all]")
    parser.add_argument('-v', '--version', action='version',
                        version="%(prog)s (version " +  VERSION + ")")
    parser.add_argument("-o", "--output", required=True,
                        help="output json file\n")
    parser.add_argument("-s", "--spid", required=False,
                        help="sequencing project id\n")
    parser.add_argument("-c", "--combined", required=False,
                        action='store_true', default=False, help="combined\n")
    parser.add_argument("--force_complete", required=False,
                        action='store_true', default=False,
                        help="use a task id even if complete\n")
    parser.add_argument("-a", "--apid", required=False,
                        help="analysis project id (overrides -s (with or without -c))\n")
    args = parser.parse_args()

    apid = 0
    if args.apid:
        apid = args.apid
    elif args.spid:
        apid = mga_spid_to_apid(args.spid, args.combined, args.force_complete)
    else:
        sys.exit("specify spid or apid or fastq")

    result = proposal_query_ap(apid)
    result["jamo_count_ap"] = get_ap_result_count_in_jamo(apid)
    result = add_library_info(result,force=args.force_complete)

    with open(args.output, "w") as write_file:
        write_file.write(json.dumps(result, indent=4))

def sequnit_to_usable(sequnit):
    '''
    Given a sequnit, return usable status
    args:sequnit
    returns: True or False
    '''
    sequnit = os.path.basename(sequnit)
    usable = None
    cmd = 'curl --fail --silent https://rqc.jgi-psf.org/api/sow_qc_info/stats?filter_SeqUnit=' + sequnit
    json_result = json.loads(subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read())
#    json_result = json.loads(os.popen(cmd).read())
    if len(json_result) == 1:
        usable = True if (json_result[0].get('USABLE') == 1) else False
    else:
        usable = None
    return usable

def sequnit_to_uniquekmer(sequnit):
    '''
    Given a sequnit, return a kmer count of the filtered file
    args:sequnit
    returns:rqc_unique kmer count
    '''
    cmd = 'curl --fail --silent https://rqc.jgi-psf.org/api/rqcws/dump/' + sequnit
    json_result = json.loads(subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read())
#    json_result = json.loads(os.popen(cmd).read())
    kmerdepth = query_dict(".stats.filter:kmer_depth_total_kmers_counted", json_result)
    return kmerdepth

def file_to_jamoid(filename):
    '''
    Given a filename, return a jamo id
    args: filename
    returns:jamoid
    '''
    filename = os.path.basename(filename)
    result = jgi_mga_utils.web_services('jamo', {'file_name':filename})
    if len(result) == 1:
        _id = result[0]["_id"]
    else:
        sys.exit("something wrong " + str(len(result)) + " results for jamo id for " + filename)
    return _id

def file_to_filtered_from_sequnit(su_,force_latest=False):
    cmd = "curl --silent --fail https://rqc.jgi-psf.org/api/rqcws/filter_fastq/" + su_
    json_result = json.loads(subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read())
    tmp = list()
    for res in json_result:
        if res['filter_fastq_status']!='OFFLINE':
            tmp.append(res)
    json_result = tmp
    if len(json_result) >1:
        sys.stderr.write("More than one filter result\n" + "See: " + cmd + "\n")
        if force_latest==False:
            sys.exit("quitting")
        else:
            return json_result[0]["filter_fastq"]
    elif len(json_result)==1:
        return json_result[0]["filter_fastq"]
    else:
        sys.exit("No Result for " +  su_ + " " + cmd)
        

def file_to_filtered_with_parent(id_, type_="Metagenome"):
    '''
    Given a raw filename, looks up a filtered filename
    args:parent_file
    returns:filtered version of parent_file
    ####### TODO: revamp to target only modern filtered. will refilter when unavailable ###########
    '''
    #get library from parent id
    data = {'inputs':id_, 'metadata.fastq_type':'filtered'}
    result = jgi_mga_utils.web_services('jamo', data)
    tmp_result = []
    filename = ""
    if type_ == "Metagenome Metatranscriptome":
        for res in result:
            if res['file_name'].find('.hR.') != -1 or res['file_name'].find('.anqrpht.') != -1:## modify for more robust filtered typing
                tmp_result.append(res)
        result = tmp_result
    elif type_ == "Metagenome Viral Standard Draft":
        result = [i for i in result if i['file_name'].find('filter-VIRAL-METAGENOME') != -1]
    elif type in ["Metagenome Standard Draft", "Metagenome Minimal Draft",
                  "Unamplified,  Random,  Cell Enrichment", "Unamplified,  Targeted,  Cell Enrichment", "Unamplified, Targeted, Cell Enrichment"]:
        if len([i for i in result if i['file_name'].find('filter-METAGENOME') != -1]):
            result = [i for i in result if i['file_name'].find('filter-METAGENOME') != -1]
        elif len([i for i in result if i['file_name'].find('anqdpht.fastq.gz') != -1]):
            result = [i for i in result if i['file_name'].find('anqdpht.fastq.gz') != -1]
        elif len([i for i in result if i['file_name'].find('anqdp.fastq.gz') != -1]):
            result = [i for i in result if i['file_name'].find('anqdp.fastq.gz') != -1]
        else:
            result = []

    if len(result) == 1:
        filename = result[0]["file_path"] + "/" + result[0]["file_name"]
    else:
        print "something wrong " + str(len(result)) + " results"
    return filename

def get_ap_result_count_in_jamo(apid):
    '''
    Given analysis, project id , return the count of files in jamo with that ap
    input:ap
    returns:count
    '''
    ap_result_count = 0
    #get library from file metadata
    data = {'metadata.analysis_project_id':apid}
    result = jgi_mga_utils.web_services('jamo', data)
    if len(result) > 0:
        ap_result_count = len(result)
    return ap_result_count

def mga_spid_to_apid(spid, combined=False, force_task_complete=False):
    '''
    Given a sequencing project id, return apid
    args: spid=sequencing project id
          combined(True/False) combined assembly
          force_task_complete=True will return task even if is complete
    returns:
          apid
    '''
    apid = 0
    ap_ = spid_to_ap_type(spid, force=force_task_complete)
    if combined and "Combined Assembly" not in ap_.keys():
        sys.exit("combined flag specified but none found")
    elif combined and ("Combined Assembly" in ap_.keys()) and len(ap_["Combined Assembly"]) == 1:
        apid = ap_["Combined Assembly"][0]
    elif "Metagenome Standard Draft" in ap_.keys() and "Metagenome Minimal Draft" in ap_.keys() and len(ap_["Metagenome Standard Draft"]) == 1:
        sys.stderr.write("Minimal and Standard exist with this spid ... going with Standard")
        apid = ap_["Metagenome Standard Draft"][0]
    elif "Metagenome Standard Draft" in ap_.keys() and len(ap_["Metagenome Standard Draft"]) == 1:
        apid = ap_["Metagenome Standard Draft"][0]
    elif "Metagenome Draft" in ap_.keys() and len(ap_["Metagenome Draft"]) == 1:
        apid = ap_["Metagenome Draft"][0]
    elif "Metagenome Minimal Draft" in ap_.keys() and len(ap_["Metagenome Minimal Draft"]) == 1:
        apid = ap_["Metagenome Minimal Draft"][0]
    elif "Unamplified, Random, Cell Enrichment" in ap_.keys():
        apid = ap_["Unamplified, Random, Cell Enrichment"][0]
    elif "Unamplified, Targeted, Cell Enrichment" in ap_.keys():
        apid = ap_["Unamplified, Targeted, Cell Enrichment"][0]
    elif "Metagenome Viral Standard Draft" in ap_.keys():
        apid = ap_["Metagenome Viral Standard Draft"][0]
    elif "Custom Analysis" in ap_.keys():
        apid = ap_["Custom Analysis"][0]
    else: #case not found, return 0 for apid
        pass

    return apid

def spid_to_ap_type(spid, force=False):
    '''
    Given spid, return ap type
    args:
      spid: sequencing project id
      force: true = return complete apids
    returns: ap type dictionary with assembly task list
      ex. {u'Metagenome Standard Draft': [1120397, 1120397]}
    '''
    data = {'sequencing_project_id':int(spid)}
    ap_ = jgi_mga_utils.web_services('ap_byspid', data)
    result = dict()
    for analysis in ap_:
        analysis = analysis.get("uss_analysis_project")
        if analysis["analysis_product_type_name"] not in result.keys():
            result[analysis["analysis_product_type_name"]] = []
        for task in analysis["analysis_tasks"]:
            if task["analysis_task_type_id"] in [47, 48]:#assembly
                if task["current_status_id"] not in [9] or force == True:
                    result[analysis["analysis_product_type_name"]].append(analysis["analysis_project_id"])
    return result

def proposal_query_ap(apid):
    '''
    Given an apid return analysis project dict
    args: analysis project id
    returns: ap related dict
    example:
    {"sequencing_projects": [{"sequencing_project_id": 1114546, "sequencing_project_name": "Su13.VD.MLB.S.D"}], "analysis_task_id": 0, "analysis_project_name": "Su13.VD.MLB.S.D", "analysis_product_type_name": "Metagenome Viral Standard Draft", "analysis_project_id": 1114494}
    '''
    data = {'analysis_project_id':int(apid)}
    ap_ = jgi_mga_utils.web_services('ap', data)
    ap_ = ap_.get("uss_analysis_project")
    result = dict()
    result["analysis_project_id"] = ap_["analysis_project_id"]
    result["analysis_project_name"] = ap_["analysis_project_name"]
    result["analysis_product_type_name"] = ap_["analysis_product_type_name"]
    result["sequencing_projects"] = ap_["sequencing_projects"]
    result["analysis_task_id"] = 0
    for task in ap_["analysis_tasks"]:
        if re.search('ssembly', task["analysis_task_type_name"]):
            if (task["current_status_id"] not in [9, 10, 13]):
                if result["analysis_task_id"]: #2nd encounter, has 2 non ["complete", "abandoned", "deleted"] assy tasks
                    sys.exit("multiple assembly tasks? check apid:" + str(apid))
                else:
                    result["analysis_task_id"] = task["analysis_task_id"]
                    result["analysis_task_state"] = task["status_name"]
                    result["analysis_task_type_name"] = task["analysis_task_type_name"]
    return result

def add_library_info(apinfo, force=False):
    '''
    Given an apid, gets sdm files and information
    args: apinfo dict (output from proposal_query_ap)
          force=true returns all information regardless of multi file, spid, or library
    output: modified apinfo dict with library information added
    '''
    libraries = list()
    files = list()
    spids = list()
    for i in range(0, len(apinfo["sequencing_projects"])):
        sdm = sdm_query_fastq_normal(apinfo["sequencing_projects"][i]["sequencing_project_id"])
        apinfo["sequencing_projects"][i]["lib"] = []
        for file_ in sdm:
            lib = {}
            lib["lib"] = file_["metadata"]["library_name"]
            lib["spid"] = file_["metadata"]["sequencing_project_id"]
            lib["file"] = file_["file_path"] + "/" + file_["file_name"]
            lib["file"].rstrip()

            libraries = list(set(libraries + [lib["lib"]])) # multi-library accounting 
            spids = list(set(spids + [lib["spid"]])) # multi-spid accounting 
            files = list(set(files + [lib["file"]])) # multi-file accounting 

            lib["file_read_count"], lib["file_base_count"] = file_to_read_count(lib["file"])

            jamoid = file_to_jamoid(lib["file"])
            name = apinfo['analysis_product_type_name']
            lib["sequnit"] = file_["file_name"]
            lib["filtered"] = file_to_filtered_from_sequnit(lib["sequnit"], force_latest=force)
            lib["filtered_read_count"] = file_to_read_count(lib["filtered"])[0]

            if "sow_segment" in file_["metadata"].keys():
                if "instrument_type" in file_["metadata"]["sow_segment"].keys():
                    lib["instrument_type"] = file_["metadata"]["sow_segment"]["instrument_type"]
                elif "sequencer_model" in file_["metadata"]["sow_segment"].keys():
                    lib["instrument_type"] = file_["metadata"]["sow_segment"]["sequencer_model"]
                elif  "physical_run" in file_["metadata"].keys() and "instrument_type" in file_["metadata"]["physical_run"]:
                    lib["instrument_type"] = file_["metadata"]["physical_run"]["instrument_type"]
                if "platform_name" in file_["metadata"]["sow_segment"].keys():
                    lib["platform_name"] = file_["metadata"]["sow_segment"]["platform_name"]
                elif "platform" in file_["metadata"]["sow_segment"].keys():
                    lib["platform_name"] = file_["metadata"]["sow_segment"]["platform"]
                elif "physical_run" in file_["metadata"].keys() and "platform_name" in file_["metadata"]["physical_run"]:
                    lib["platform_name"] = file_["metadata"]["physical_run"]["platform_name"]
                lib["library_protocol"] = file_["metadata"]["sow_segment"]["library_protocol"]
                lib["target_fragment_size_bp"] = file_["metadata"]["sow_segment"]["target_fragment_size_bp"]
            else:
                lib["instrument_type"] = "NA"
                lib["platform_name"] = "NA"
                lib["library_protocol"] = "NA"
                lib["target_fragment_size_bp"] = "NA"

            if "proposal" in file_["metadata"].keys():
                lib["proposal_id"] = file_["metadata"]["proposal"]["id"]
                lib["proposal_name"] = ''.join([x for x in file_["metadata"]["proposal"]["title"] if x in string.printable])
            else:
                lib["proposal_id"] = "NA"
                lib["proposal_name"] = "NA"
            lib["usable"] = sequnit_to_usable(lib["sequnit"])
            apinfo["sequencing_projects"][i]["lib"].append(lib)
    if force == False:
        if len(libraries) != 1:
            sys.exit("multiple libraries attempted, (run offline for now): " + ", ".join(libraries))
        if len(files) != 1:
            sys.exit("multiple files attempted (run offline for now): " + ", ".join(files))
        if len(spids) != 1:
            sys.exit("multiple spids attempted (run offline for now): " + ", ".join(spids))
    return apinfo

def file_to_read_count(full_file):
    '''
    Given a fastq file, return read count
    args: fastq file name
    return: read count, base count
    '''
    su_ = os.path.basename(full_file)
    read_count = 0
    base_count = 0
    result = jgi_mga_utils.web_services('jamo', {'file_name':su_})
    if len(result) > 1:
        read_count = "Multiple files"
    else:
        type_ = query_dict('metadata.fastq_type', result[0])
        if type_ == "filtered":
            read_count = query_dict('metadata.filter_reads_count', result[0])
            base_count = "NA" #base count not stored in fitered metadata
        elif type_ == "sdm_normal":
            read_count = query_dict('metadata.rqc.read_qc.read_count', result[0])
            base_count = query_dict('metadata.rqc.read_qc.base_count', result[0])
        else:
            read_count = "NA"
    if read_count == "__NA__" and os.path.exists(full_file):#do a manual count
        cmd = "zcat " + full_file + "  | wc -l"
        read_count = int(subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read())
        read_count = read_count/4
        base_count = "NA"
    return [read_count, base_count]

def query_dict(query, hash_):
    '''
    Given a query, traverse a hash and retun value or not found
    args: query string (ex 'metadata.sequencing_project_id'), dict to query
    returns: the query value or "__NA__"
    '''
    query = query.lstrip(".") # initial . in "jq like" queries is allowed
    if "." in query:      #still some levels
        fields = query.split(".")
        top_field = fields.pop(0)
        try:
            return_value = query_dict(".".join(fields), hash_[top_field])
        except:
            return_value = "__NA__"
    else:
        if hash_[query] != collections.defaultdict():
            return_value = hash_[query]
        else:
            return_value = "__NA__"
    return return_value

def sdm_query_fastq_normal(id_):
    '''
    Given a library id, return jamo record
    args: id
    returns: jamo record
    '''
    stringid = str(id_)
    if re.match(r'^[A-Z]{4,4}$', stringid): #lib id
        result = jgi_mga_utils.web_services('jamo', {'metadata.library_name':id_, 'metadata.fastq_type':'sdm_normal'})
    elif re.match(r'^H\d+$', stringid): #offline
        result = jgi_mga_utils.web_services('jamo', {'metadata.library_name':stringid})
    elif re.match(r'^\d{7,7}$', stringid): #spid
        result = jgi_mga_utils.web_services('jamo', {'metadata.sequencing_project_id':int(id_), 'metadata.fastq_type':'sdm_normal'})
    else:
        sys.exit(id + " type not recognized as library or spid")
    return result

if __name__ == "__main__":
    main()

