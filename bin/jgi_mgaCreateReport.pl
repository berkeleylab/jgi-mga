#!/usr/bin/env perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin";
use File::Basename;
use Text::Wrap;
$Text::Wrap::columns=100;
use JSON;
use Data::Dumper;
use Cwd 'abs_path';

umask 002;
unless (@ARGV>=3){
    die "Usage: $0 <info><metadata.json> <outdir> [optional readme text body]\n";
}

my $optJsonInfo = $ARGV[0];
my $metadataJson = $ARGV[1];
my $metadataJsondir = dirname($metadataJson);
my $outdir = $ARGV[2];
if (! -d $outdir && ! -w $outdir){
    warn "$outdir not writable \n";
}
 
my $metadata = &get_json($metadataJson);
my $readmeMethodsFile = $ARGV[3];

my $statstxt = &get_readme_stats($optJsonInfo,$metadataJson);
my $covfile = &get_metadata_file_location($metadataJson,"metagenome_assembly_coverage");

#get versions for readme
my $pipeline_version = `jgi_mga_meta_rqc.py --version 2>&1`;
chomp $pipeline_version;
my $assembly_metadata = &get_metadata_output($metadataJson,"contigs");
my $assembler = $assembly_metadata->{'metadata'}{'assembler'};
my $assembler_version = $assembly_metadata->{'metadata'}{'assembler_version'};
my $assembler_parameters = $assembly_metadata->{'metadata'}{'assembler_parameters'};

my $bfc_metadata = &get_metadata_output($metadataJson,"reads_filtered");
my $bfc = $bfc_metadata->{'metadata'}{'corrector'};
my $bfc_version = $bfc_metadata->{'metadata'}{'corrector_version'};
my $bfc_parameters = $bfc_metadata->{'metadata'}{'corrector_parameters'};

my $bbmap = &get_metadata_output($metadataJson,"metagenome_alignment");
my $bbmap_version = $bbmap->{'metadata'}{'aligner_version'};


my $readmeMethods = &get_readme_methods($readmeMethodsFile,$assembler);
$readmeMethods =~ s/__PIPELINE_VERSION__/\($pipeline_version\)/;
$readmeMethods =~ s/__ASSEMBLER_PARAMETERS__/\"${assembler_parameters}\"/;
$readmeMethods =~ s/__ASSEMBLER_VERSION__/\(${assembler_version}\)/;
$readmeMethods =~ s/__BBMAP_VERSION__/\(version $bbmap_version\)/;
$readmeMethods =~ s/__BFC_VERSION__/\(version $bfc_version\)/;
$readmeMethods =~ s/__BFC_PARAMETERS__/\"$bfc_parameters\"/;

my @graphics = &generate_graphics($covfile,$outdir,"report");
my ($txtReport,$pdfReport) = &generate_pdf($statstxt,\@graphics,$readmeMethods,$outdir,"report");



sub generate_pdf{
    my $txt = $_[0];
    my @graphics = @{$_[1]};
    my $methods = $_[2];
    my $outdir = $_[3];
    my $prefix = $_[4];
    my $txtout = "$outdir/${prefix}.release.txt";
    my $htmlout = "$outdir/${prefix}.release.html";
    my $pdfout = "$outdir/${prefix}.release.pdf";
    open(OUT, ">$txtout") or die "Can't open $txtout\n";
    print OUT $txt;
    print OUT $methods;
    close(OUT);
    open( OUT, ">$htmlout") or die "Can't open $htmlout\n";
    print OUT <<EOM;
    <html>
	<head>
	</head>
	<body bgcolor="#ffffff" text="#000000" link="#0000ff" vlink="#ff0000" alink="#ffff00">
	<pre>
EOM
    print OUT $txt;
    print OUT $methods;
    print OUT "        </pre>\n";
    foreach my $image (@graphics){
	print OUT "<img src=\"$image\"\/>\n";
    }
    print OUT <<EOM;
        </body>
    </html>
EOM
    close(OUT);
    my $cmd = "$FindBin::Bin/wkhtmltopdf $htmlout $pdfout 1> ${pdfout}.o 2> ${pdfout}.e";
    unless(! system($cmd)){
	die "could not convert $cmd\n";
    }
    return ($txtout,$pdfout);
#    $cmd = "$FindBin::Bin/uuencode ${prefix}.release.pdf ${prefix}.release.pdf > ${prefix}.release.uu";
#    unless(! system($cmd)){
#	die "could not convert $cmd\n";
#    }

}

sub get_spids{
    my $info = $_[0];
    my @spids = ();
    for(my $i=0;$i<scalar(@{$info->{'sequencing_projects'}});$i++){
        push(@spids,$info->{'sequencing_projects'}[$i]{'sequencing_project_id'});
    }
    @spids = sort {$a <=> $b} @spids;
    return @spids;
}

sub get_proposal{
    my $info = $_[0];
    my %proposal = (); 
    for(my $i=0;$i<scalar(@{$info->{'sequencing_projects'}});$i++){
        for(my $j=0;$j<scalar(@{$info->{'sequencing_projects'}[$i]{'lib'}});$j++){
	    $proposal{'proposal_name'}{$info->{'sequencing_projects'}[$i]{'lib'}[$j]{'proposal_name'}}++;
	    $proposal{'proposal_id'}{$info->{'sequencing_projects'}[$i]{'lib'}[$j]{'proposal_id'}}++;

        }
    }
    return %proposal;
}


sub get_readme_stats{
    my $info_file = $_[0];
    my $metadata_json_file = $_[1];

    my $readme ="";
    my $info = &get_json($info_file);
    my $spids = join(" ",&get_spids($info));
    my $metadata = &get_json($metadata_json_file);
    

    $readme .= "JGI assembly of "  . $metadata->{'metadata'}{'subtitle'} . "\n\n";


    my %proposal = &get_proposal($info);
    $readme .= "Proposal Name: " . wrap("\t","",(keys %{$proposal{'proposal_name'}})[0]) . "\n";
    $readme .= "Proposal ID: " . (keys %{$proposal{'proposal_id'}})[0] . "\n";
    $readme .= "Analysis Project/Task ID: " . $info->{'analysis_project_id'} . "/" . $info->{'analysis_task_id'} . "\n";
    $readme .= "Sequencing Project ID(s): " . wrap("\t","",$spids) . "\n\n"; 
    $readme .= "Input Data:\n" . &library_readme($info,$metadata);
    
    $readme .= "Read Pre-processing:\n";

    my $raw = $metadata->{'metadata'}{'raw_reads_n'}[0];
    my $filt = $metadata->{'metadata'}{'filt_reads_n'}[0];
    my $final = $metadata->{'metadata'}{'input_reads_n'}[0];
    my $filt_pct_raw = sprintf("%.1d", ($filt/$raw*100));
    my $final_pct_raw = sprintf("%.1d", ($final/$raw*100));

    $readme .= "The number of raw input reads is: $raw\n";
    $readme .= "The number of read remaining after quality filter: $filt  (${filt_pct_raw}% of raw)\n";
    $readme .= "The final number of reads remaining after read correction: $final (${final_pct_raw}% of raw)\n\n";
    
    $readme .= "Assembly Stats:\n";
    my $dir = &get_metadata_file_location($metadata_json_file,"contigs");
    $dir=~s/^(.*)\/.*$/$1/;
    my $cmd =  "cat $dir/../assembly_stats/assembly.scaffolds.fasta.stats.txt";
    my $stats = `$cmd`;
    $readme .=  $stats;

    
    $readme .= "Alignment of reads to final assembly:\n";
    my ($aligned_readcount_input,$aligned_readcount,$perc_aligned)= &alignment_count($metadata_json_file);
    my ($m50, $m90) = &get_m50_m90($metadata_json_file);
    $readme .= "The number of reads used as input to aligner is: $aligned_readcount_input\n";
    $readme .= "The number of aligned reads is: $aligned_readcount ($perc_aligned%)\n";
    $readme .= "m50/m90 (length where 50% or 90% of reads align to contigs of this length or larger is: $m50/$m90\n\n";
    
#    $readme .= "Unaligned Merged Reads:\n"; 
#    my ($merged_readcount_input,$merged_readcount,$perc_merged)= &merge_count($metadata_json_file);   
#    $readme .= "The number of unmapped read pairs as input to bbmerge is: $merged_readcount_input\n";
#    $readme .= "The number of merged read pairs is : $merged_readcount ($perc_merged%)\n\n";
    return $readme;
}

sub get_m50_m90(){
    my $metadataJson = $_[0];
    #get aligner input reads count
    my $metadata = &get_metadata_output($metadataJson,"metagenome_alignment");
    my $input_readcount=$metadata->{'metadata'}{'num_input_reads'};

    $metadata = &get_metadata_output($metadataJson,"metagenome_assembly_coverage");
    (my $file = $metadata->{'file'})=~s/:.*$//;

    my $sum=0;
    my $m50="NA";
    my $m90="NA";
    my @F;
    open(IN, "$file") or die "Can't open $file\n";
    while(<IN>){
        @F=split(/\t/,$_);
        next unless $F[6]=~/^\d+$/;
        $sum+=$F[6] + $F[7];
        if ($sum/$input_readcount >0.5 && $m50 eq "NA"){
            $m50=$F[2];
        }
        if ($sum/$input_readcount >0.9 && $m90 eq "NA"){
            $m90=$F[2];
        }
    }
    close(IN);
    return ($m50,$m90);
}

sub merge_count{
    my $metadataJson = $_[0];
    my $output = &get_metadata_output($metadataJson,"metagenome_unmapped_merged");
    my $perc_merged = sprintf ("%.2f",$output->{'metadata'}{'readcount'} / $output->{'metadata'}{'readcount_input'}*100);
    return ($output->{'metadata'}{'readcount_input'}, $output->{'metadata'}{'readcount'},$perc_merged); 
}

sub alignment_count{
    my $metadataJson = $_[0];
    my $metadata = &get_metadata_output($metadataJson,"metagenome_alignment");
    my $numinputreads =  $metadata->{'metadata'}{'num_input_reads'};
    my $aligned = $metadata->{'metadata'}{'num_aligned_reads'};
    my $perc_aligned = sprintf ("%.2f",$aligned /$numinputreads *100);
    return ($numinputreads,$aligned,$perc_aligned); 
}

sub get_assembly_stats{
    my $file = $_[0];
    my $cmd = "module load bbtools; stats.sh $file";
    my $stats = `$cmd`;
    return $stats;
}

sub get_metadata_file_location{
    my $metadataFile = $_[0];
    my $output_label = $_[1];
    my $metadataBasename = &dirname(abs_path($metadataFile));
    my $output = &get_metadata_output($metadataFile,$output_label);
    $output->{'file'} =~ s/\:.*$//;
    return &abs_path($output->{'file'}); 
}

sub get_metadata_output{
    my $metadataFile = $_[0];
    my $output_label = $_[1];
    my $metadata = &get_json($metadataFile);
    my @output = ();
    for (my $i=0;$i<scalar(@{$metadata->{'outputs'}});$i++){
	if ($metadata->{'outputs'}[$i]{'label'} eq $output_label){
	    push (@output,$metadata->{'outputs'}[$i]);
	}
    }
    if (scalar(@output !=1)){
	die "cound not find unique file path for $output_label\n";
    }
    return $output[0];
}

sub library_readme{
    my $info = $_[0];
    my $metadata= $_[1];
    my $readme = "";
    my %used_sequnits;
    foreach my $sequnit (@{$metadata->{'metadata'}{'seq_unit_name'}}){
	$used_sequnits{$sequnit}++;
    }
    for(my $i=0;$i<scalar(@{$info->{'sequencing_projects'}});$i++){
        for(my $j=0;$j<scalar(@{$info->{'sequencing_projects'}[$i]{'lib'}});$j++){
	    my $lib = $info->{'sequencing_projects'}[$i]{'lib'}[$j];
	    next unless (exists $used_sequnits{$lib->{'sequnit'}});
	    my $tfs = (exists $lib->{'target_fragment_size_bp'} && $lib->{'target_fragment_size_bp'}) ? $lib->{'target_fragment_size_bp'} : "NA";
	    $readme .= "Library: " . $lib->{'lib'} . "\n";
	    $readme .= "Platform: " . $lib->{'platform_name'} . " " . $lib->{'instrument_type'} . " " . $lib->{'library_protocol'} . " " . $tfs . " bp fragment\n" ;
	    $readme .= "FilteredData: " . $lib->{'filtered'} . "\n";
	    $readme .= "RawData: " . $lib->{'file'} . "\n\n";
        }
    }
    return $readme;
}

sub generate_graphics{
    my $coverageFile = $_[0];
    my $outDir = $_[1];
    my $prefix = $_[2];

    
    #graphics routine from LANL
    my $file_name = $prefix;
    my $rscript = "$outDir/$prefix.R";
    open (Rscript, ">$rscript") or die "can't open $rscript\n";
    print Rscript"
a<-read.table(file=\"${coverageFile}\",header=TRUE,comment=\"\",sep=\"\\t\")
bitmap(\"$outDir/${file_name}_avg_fold_vs_len.bmp\")
plot(a\$Length,a\$Avg_fold,pch=8,xlab=\"Contigs Length (bp)\",ylab=\"Average coverage fold (x)\",cex=0.7, main=\"Contigs average fold coverage vs. Contigs Length\",log=\"y\")
tmp<-dev.off();

bitmap(\"$outDir/${file_name}_gc_vs_avg_fold.bmp\");
par(mar = c(5, 5, 5, 5), xpd=TRUE, cex.main=1.5, cex.lab=1.2)
plot(a\$Avg_fold,a\$Ref_GC,ylab=\"GC (%)\",xlab=\"Average coverage fold (x)\", main=\"Contigs average fold coverage vs. GC\",pch=20,col=\"blue\",cex=log(a\$Length/mean(a\$Length)),log=\"x\");
tmp<-dev.off();

bitmap(\"$outDir/${file_name}_gc.bmp\");hist(subset(a,a\$Length>0)\$Ref_GC*100,seq(from=0,to=102,by=1.5),main=\"GC Histogram for contigs\",ylab=\"\# of contigs\",xlab=\"GC (%)\");dev.off()

quit();
";
    if (system ("module load R && R --vanilla --slave --silent < $rscript 1> ${rscript}.o 2> ${rscript}.e")) {warn "$!\n"};
    return "$outDir/${prefix}_avg_fold_vs_len.bmp", "$outDir/${prefix}_gc_vs_avg_fold.bmp","$outDir/${prefix}_gc.bmp";
}

sub get_json{
    my $file = $_[0];
   # get json info file                                                                                                                                               
    open (IN, $file) or die "can't open $file\n";
    my @info = <IN>;
    close(IN);
    chomp @info;
    my $info = &decode_json(join("",@info));
    return $info;
}

sub get_readme_methods{
    my $file = $_[0];
    my $assembler = $_[1];
    my @txt = ();
    my $txt = "";
    if ($file){
	my $tmp;
        open (IN,$file) or die "can't open $file\n";
        while($tmp=<IN>){
            chomp;
            $txt .= $tmp;
        }
        close(IN);
	return $txt;
    }
    if (lc($assembler) eq "megahit"){
	$txt = <<"EOT";
Assembly Methods:
    Trimmed, screened, paired-end Illumina reads (see documentation for bbtools(1) filtered reads) were assembled using megahit assembler (2) using a range of Kmers. Megahit parameters was used with the following options: "--k-list 23,43,63,83,103,123"

    The entire filtered read set was mapped to the final assembly and coverage information generated using bbmap(3) using default parameters exept ambiguous=random.

If you have any questions, please let us know: Brian Foster bfoster\@lbl.gov, Alicia Clum aclum\@lbl.gov, Alex Copeland accopeland\@lbl.gov


  (1) B. Bushnell: BBTools software package, http://bbtools.jgi.doe.gov
  (2) megahit  __ASSEMBLER_VERSION__ https://github.com/voutcn/megahit: Li, D., et al. (2015) MEGAHIT: An ultra-fast single-node solution for large and complex metagenomics assembly via succinct de Bruijn graph. Bioinformatics, doi: 10.1093/bioinformatics/btv033 [PMID: 25609793].
  (3) bbmap.sh __BBMAP_VERSION__ https://bbtools.jgi.doe.gov: ambiguous=random
EOT
    }
    elsif(lc($assembler) eq "spades"){
	$txt = <<"EOT";
Assembly Methods:
    Trimmed, screened, paired-end Illumina reads (see documentation for bbtools(1) filtered reads) were read corrected using bfc __BFC_VERSION__ with __BFC_PARAMETERS__ (2). Reads with no mate pair were removed.

    The resulting reads were then assembled using SPAdes assembler __ASSEMBLER_VERSION__ (3) using a range of Kmers with the following options: __ASSEMBLER_PARAMETERS__

    The entire filtered read set was mapped to the final assembly and coverage information generated using bbmap __BBMAP_VERSION__ (4) using default parameters exept ambiguous=random.

    The version of the processing pipeline is __PIPELINE_VERSION__.

If you have any questions, please contact the JGI project manager.


  (1) B. Bushnell: BBTools software package, http://bbtools.jgi.doe.gov
  (2) BFC: correcting Illumina sequencing errors. - Bioinformatics. 2015 Sep 1;31(17):2885-7. doi: 10.1093/bioinformatics/btv290. Epub 2015 May 6.
  (3) metaSPAdes: a new versatile metagenomic assembler - Genome Res. 2017. 27: 824-834.  doi: 10.1101/gr.213959.116
  (4) bbmap.sh https://bbtools.jgi.doe.gov
EOT
    }
    $txt = join("\n",map { wrap("","",$_)} split(/\n/,$txt));
    return $txt;
}

