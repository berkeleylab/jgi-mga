#!/usr/bin/env python

'''
jgi_mga_spades_ram.py
Takes a kmer count or file of reads for positional argument
Outputs memory prediction to stdout or to file

Note:
currently uses linear regression on post bfc reads kmer count and spades version 3.10.1

Todo:
redo linear regression on post rqcfilter kmer count and spades version 3.10.1

depends on "module load bbtools;kmercountexact.sh" if file of reads is given
'''

import sys
import os
import argparse
import subprocess
VERSION = "1.0.0"

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='determine ram given readfile or kmercount.')
    parser.add_argument("kmer")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " +  VERSION + ")")
    parser.add_argument("-k", "--kmerlength", default=33,
                        required=False, help="[Optional] kmer length default 33\n")
    parser.add_argument("-s", "--spades_version", default="3.10.1",
                        required=False, help="[Optional] spades version. default 3.10.1\n")
    parser.add_argument("-w", "--workingdir", default="./",
                        required=False, help="[Optional] directory to store intermediate files\n")
    parser.add_argument("-o", "--output",
                        required=False, help="[Optional] file to output ram prediction default is stdout\n")
    parser.add_argument("-d", "--debug",
                        required=False, default=False, help="[Optional] print debugging information\n")
    args = parser.parse_args()


    if args.spades_version == "3.10.1":
        #(Intercept) -2.200e+01 2.093e+01 -1.051 0.298
        #data$kmercount 6.235e-08 3.168e-09 19.680 <2e-16 ***
        intercept = -2.200e+01
        slope = 6.235e-08
    else:
        sys.exit("spades 3.10.1 is the only version supported")

    if args.output:
        args.output = args.workingdir.rstrip("/") + '/' + args.output
        if not os.access(os.path.dirname(args.output), os.W_OK):
            sys.exit(args.output + " not writable\n")


    #kmer = 12099057932
    kmercount = float()
    if str(args.kmer).isdigit():
        kmercount = float(args.kmer)
    elif os.path.exists(args.kmer):
        khistfile = args.workingdir.rstrip("/") + "/khist.txt"
        if not os.access(os.path.dirname(khistfile), os.W_OK):
            sys.exit(khistfile + " not writable\n")
        cmd = ""
        if os.environ.get('NERSC_HOST') == 'genepool':
            cmd = "module load bbtools; "
        cmd += "kmercountexact.sh overwrite=t " + " k=" + str(args.kmerlength)+ " histheader=f in=" + args.kmer + " khist=" + khistfile
        cmd += " 1>| " + khistfile + ".o 2>| " + khistfile + ".e"
        proc = subprocess.Popen(cmd, shell=True)
        (output, err) = proc.communicate()
        if args.debug:
            sys.stdout.write(output)
            sys.stdout.write(err)
        p_status = proc.wait()
        if p_status == 0:
            with open(khistfile, "r") as readfile:
                for line in readfile.readlines():
                    kmercount += float(line.rstrip().split()[-1])
        else:
            sys.exit(cmd + " exited with error " + p_status)
    else:
        sys.exit("positional argument should be kmercount or file of reads")

    predicted = intercept +  (kmercount * slope)
    if args.output:
        with open(args.output, "w") as writefile:
            writefile.write(str(int(predicted)) + "\n")
    else:
        sys.stdout.write(str(int(predicted)) + "\n")

if __name__ == "__main__":
    main()
