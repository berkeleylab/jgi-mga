#!/usr/bin/env python
'''
jgi_mga_stats.py
takes a completed jgi_mga runfolder and creates key-value pair files:
  rqc-stats.txt
  rqc-files.txt

'''


from __future__ import division
import argparse
import sys
import os
import json
import re
import time
import urllib2
import subprocess

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/../lib')
import jgi_mga_utils

VERSION = "1.0.0"

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-d", "--dir", required=True, help="dir.\n")
    parser.add_argument("-s", "--statsoutput", required=True, help="key value pairs file.\n")
    parser.add_argument("-f", "--filesoutput", required=True, help="key value pairs file.\n")


    args = parser.parse_args()
    args.dir = os.path.realpath(args.dir)
    args.dir = args.dir + '/'
    if os.path.exists(args.dir) == 0:
        sys.exit()
    if args.statsoutput.startswith("/") or  args.filesoutput.startswith("/"):
        sys.exit("files should not be absolute paths")

    metadatafile = jgi_mga_utils.get_glob(args.dir + "metadata.json")
    with open(metadatafile, "r") as readfile:
        metadata = json.loads(readfile.read())

    kvp = dict()
    kvp["stats.date"] = time.strftime("%Y%m%d")
    kvp["pipeline_version"] = subprocess.Popen('jgi_mga_meta_rqc.py --version 2>&1', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).stdout.read().rstrip()


    # agp, fasta, cov counts add up
    agp = get_output_file(metadata, "metagenome_assembly_agp")
    scaffolds_fasta = get_output_file(metadata, "scaffolds")
    cov = get_output_file(metadata, "metagenome_assembly_coverage")
    kvp["sanity_check.fasta_agp_cov_consistent"] = fasta_agp_cov_consistent(agp, scaffolds_fasta, cov)

    #input files count consistent
    #config = jgi_mga_utils.get_glob(args.dir + "*.config") metadata.json populated from config
    readme = get_output_file(metadata, "text_report")
    kvp["sanity_check.metadata_config_readme_inputs_consistent"] = metadata_config_readme_inputs_consistent(metadata, readme)

    # file size > 0
    kvp["sanity_check.file_size_check"] = file_size_check(metadata, 1000)
    kvp["sanity_check.release_spid_in_jamo"] = ",".join([str(spid) for spid in release_spid_in_jamo(metadata)])
    kvp["sanity_check.release_ap_in_jamo"] = ",".join([str(apid) for apid in release_ap_in_jamo(metadata)])
    kvp["sanity_check.release_ap_to_taxonoid"] = its_ap_to_taxonoid(metadata)

    #reads filtered count
#    kvp["readstats.raw"] = metadata['metadata']['read_accounting']['raw']
#    kvp["readstats.filtered"] = metadata['metadata']['read_accounting']['rqc_filtered']
#    kvp["readstats.error_corrected"] = metadata['metadata']['read_accounting']['final_input']


    #metadata.json file check
    kvp["rqc_jat_check"] = metadata_json_check(metadatafile)

    #add all output metadata
    for output in metadata["outputs"]:
        if "metadata" in output.keys():
            for key in output["metadata"].keys():
                kvp["outputs." + output["label"] + "." + key] = output["metadata"][key]
    #add SIGS table to kvp
    kvp.update(get_sigs_table(metadata, kvp))

    #write stats
    with open(args.dir + "/" + args.statsoutput, "w") as writefile:
        for key in sorted(kvp.keys()):
            string_ = str()
            try:
                string_ = str(kvp[key])
            except:
                string_ = kvp[key].encode('utf-8')
            writefile.write(key + "=" + string_ + "\n")


    files = dict()
    reportfinal = get_output_file(metadata, "text_report", 1)
    reportdir = os.path.dirname(get_output_file(metadata, "text_report"))

    files[os.path.basename(reportfinal)] = reportfinal
    files[os.path.basename(os.path.splitext(reportfinal)[0] + ".pdf")] = os.path.splitext(reportfinal)[0] + ".pdf"
    files["report_avg_fold_vs_len.bmp"] = reportdir + "/report_avg_fold_vs_len.bmp"
    files["report_gc.bmp"] = reportdir + "/report_gc.bmp"
    files["report_gc_vs_avg_fold.bmp"] = reportdir + "/report_gc_vs_avg_fold.bmp"
    files["report.release.html"] = reportdir + "/report.release.html"

    #write files
    with open(args.dir + "/" + args.filesoutput, "w") as writefile:
        for key in files.keys():
            writefile.write(key + "=" + files[key] + "\n")
    return

def metadata_json_check(metadata_file):
    '''
    Given metadata.json file, run rqc jat sanity check
    args: metadata.json file
    returns: true if pass false if fail or None if not on genepool
    '''
    return_val = None
    host = os.environ.get('NERSC_HOST')
    if host == "genepool":
        cmd = "module load jgi-rqc;/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/jat_check.py -f " + metadata_file
        sub_p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (output, err) = sub_p.communicate()
        p_status = sub_p.wait()
        if p_status == 0:
            return_val = True
        else:
            return_val = False
    return return_val

def get_sigs_table(metadata, kvp):
    '''
    Given metadata, and key value pairs,  create sigs table for rqc stats.
    args: metadata data structure
    returns: modified kvp with "assy.SIGS" table added"

    Table 3. Library information.
    Sample Label(s)
    Sample prep method
    Library prep method(s)
    Sequencing platform(s)
    Sequencing chemistry
    Sequence size (GBp)
    Number of reads
    Single-read or paired-end sequencing
    Sequencing library insert size
    Average read length
    Standard deviation for read length

    Table 4. Sequence processing
    Tool(s) used for quality control
    Number of sequencs removed by quality control procedures
    Number of sequences that passed quality control procedures
    Number of artificial duplicate reads

    Table 5. Metagenome statistics
    Libraries Used
    Assembly tool(s) used
    Number of contigs after assembly
    Number of singletons after assembly
    minimal contig length
    Total bases assembled
    Contig n50
    % of Sequences assembled
    Measure for % assembled (either from assembly output or via mapping of reads to contigs; in the latter case provide tool and parameters used)

    
#    Sequence size (GBp)
#    Number of reads
#    Average read length
#    Standard deviation for read length
#    Number of sequences removed by quality control procedures
#    Number of sequences that passed quality control procedures
#    Number of artificial duplicate reads
#    Number of singletons after assembly
#    Contig n50
#    % of Sequences assembled
#    Measure for % assembled (either from assembly output or via mapping of reads to contigs; in the latter case provide tool and parameters used)
    '''

    #SIG table 3
    kvp["assy.SIG.3.Sample_Labels"]=""
    kvp["assy.SIG.3.Sample_prep_method"]=""
    kvp["assy.SIG.3.Library_prep_methods"]=""
    kvp["assy.SIG.3.Sequencing_platforms"]=""
    kvp["assy.SIG.3.Sequencing_chemistry"]=""
    kvp["assy.SIG.3.Sequence_size_GBp"] = 0
    for count in metadata["metadata"]["raw_reads_bp"]:
        kvp["assy.SIG.3.Sequence_size_GBp"] += count/1000000000
    kvp["assy.SIG.3.Sequence_size_GBp"] = "{:.1f}".format(kvp["assy.SIG.3.Sequence_size_GBp"])
    kvp["assy.SIG.3.Number_of_reads"] = ",".join([str(int(x)) for x in metadata["metadata"]["raw_reads_n"]])
    kvp["assy.SIG.3.Single-read_or_paired-end_sequencing"]=""
    kvp["assy.SIG.3.Sequencing_library_insert_size"]=""
    kvp["assy.SIG.3.Average_read_length"] = ",".join([str(x) for x in metadata["metadata"]["input_len_avg_bp"]])
    kvp["assy.SIG.3.Standard_deviation_for_read_length"] = ",".join([str(x) for x in metadata["metadata"]["input_len_stdev"]])
    #get Jamo info from library 
    for library in metadata["metadata"]["library_name"]:
        records = jgi_mga_utils.sdm_query_fastq_normal(library)
        for record in records:
            kvp["assy.SIG.3.Sample_Labels"] += ", " + record['metadata']['sow_segment']['sample_name']
            kvp["assy.SIG.3.Sample_Labels"] = kvp["assy.SIG.3.Sample_Labels"].lstrip(", ")
            kvp["assy.SIG.3.Library_prep_methods"] += ", " + record['metadata']['sow_segment']['actual_library_creation_queue']
            kvp["assy.SIG.3.Library_prep_methods"] = kvp["assy.SIG.3.Library_prep_methods"].lstrip(", ")
            kvp["assy.SIG.3.Sequencing_platforms"] += ", " + record['metadata']['physical_run']['platform_name']
            kvp["assy.SIG.3.Sequencing_platforms"] = kvp["assy.SIG.3.Sequencing_platforms"].lstrip(", ")
            if kvp["assy.SIG.3.Sequencing_platforms"].find("llumina") != -1:
                kvp["assy.SIG.3.Sequencing_chemistry"] = "SBS"
            kvp["assy.SIG.3.Single-read_or_paired-end_sequencing"] += ", " + record['metadata']['physical_run']['physical_run_type']
            kvp["assy.SIG.3.Single-read_or_paired-end_sequencing"] = kvp["assy.SIG.3.Single-read_or_paired-end_sequencing"].lstrip(", ")
            kvp["assy.SIG.3.Sequencing_library_insert_size"] += ", " + str(record['metadata']['rqc']['read_qc']['illumina_read_insert_size_avg_insert'])
            kvp["assy.SIG.3.Sequencing_library_insert_size"] = kvp["assy.SIG.3.Sequencing_library_insert_size"].lstrip(", ")
            
    #SIG table 4
    kvp["assy.SIG.4.Tools_used_for_quality_control"] = "\"" + kvp["outputs.reads_filtered.corrector"] + " version " + kvp["outputs.reads_filtered.corrector_version"] + " was run on rqc filtered files with the following parameters " + kvp["outputs.reads_filtered.corrector_parameters"] + "\""

    kvp["assy.SIG.4.Number_of_sequences_removed_by_quality_control_procedures"] = int(sum(metadata["metadata"]["raw_reads_n"]) - sum(metadata["metadata"]["input_reads_n"]))
    kvp["assy.SIG.4.Number_of_sequences_that_passed_quality_control_procedures"] = int(sum(metadata["metadata"]["input_reads_n"]))
    kvp["assy.SIG.4.Number_of_artificial_duplicate_reads"] = "NA"

    #SIG table 5
    kvp["assy.SIG.5.Libraries_Used"] = ",".join(metadata["metadata"]["library_name"])
    kvp["assy.SIG.5.Assembly_tools_used"] = kvp["outputs.contigs.assembler_version"]
    kvp["assy.SIG.5.Number_of_contigs_after_assembly"] = kvp["outputs.contigs.n_contigs"]
    kvp["assy.SIG.5.Number_of_singletons_after_assembly"] = ""
    kvp["assy.SIG.5.minimal_contig_length"] = 200
    kvp["assy.SIG.5.Total_bases_assembled"] = kvp["outputs.contigs.contig_bp"]
    kvp["assy.SIG.5.Contig_n50"] = kvp["outputs.contigs.ctg_N50"]
    kvp["assy.SIG.5.percent_of_Sequences_assembled"] = ""
    kvp["assy.SIG.5.Measure_for_percent_assembled"] = ""

    covstats = "../readMappingPairs/covstats.txt"
    total_reads_input_to_aligner = 0
    for output in metadata["outputs"]:
        if output['label']=="metagenome_alignment":
            total_reads_input_to_aligner = output["metadata"]["num_input_reads"]
    sum_aligned = 0
    sum_aligned_gt10k = 0
    with open(covstats, "r") as readfile:
        for line in readfile:
            if line.startswith("#"):
                continue
            fields = line.split("\t")
            sum_aligned += int(fields[6]) + int(fields[7])
            if int(fields[2]) > 10000:
                sum_aligned_gt10k += int(fields[6]) + int(fields[7])
    perc_aligned = "{:.1f}".format(float(sum_aligned/float(total_reads_input_to_aligner)*100))
    perc_gt10k_aligned = "{:.1f}".format(float(sum_aligned_gt10k/float(total_reads_input_to_aligner)*100))
    kvp["assy.SIG.5.percent_of_Sequences_assembled"] = perc_aligned
    kvp["assy.SIG.5.percent_of_Sequences_assembled_gt10k"] = perc_gt10k_aligned
    kvp["assy.SIG.5.Measure_for_percent_assembled"] = "\"filtered reads were were mapped back to the assembled reference using aligner " + kvp["outputs.metagenome_alignment.aligner"] + " version " + kvp["outputs.metagenome_alignment.aligner_version"] + " with options ambiguous=random.\""
    kvp["assy.SIG.5.Number_of_singletons_after_assembly"] =  total_reads_input_to_aligner - sum_aligned
    return kvp

def get_output_file(metadata, key, index=0):
    '''
    Given metadata key and index, return all the output files
    args: metadata.json data structure
          label of the output
    returns:
          output file path
    '''
    output_file = ""
    for output in metadata["outputs"]:
        if output["label"] == key:
            output_file = output["file"]
            output_file = output_file.split(":")[index]
    return output_file

def its_ap_to_taxonoid(metadata):
    '''
    args: metadata.json data strucure
    returns:taxonoid for apid
    '''
    oid = ""
    apid = ""
    if "analysis_project_id" in metadata["metadata"].keys():
        if len(metadata["metadata"]["analysis_project_id"]) == 1:
            apid = metadata["metadata"]["analysis_project_id"][0]

    #url = "https://gold.jgi-psf.org/rest/analysis_project/" + str(apid)
    url = "https://gold.jgi.doe.gov/rest/analysis_project/" + str(apid)
    #authKey=base64.b64encode(username + ':' + password)
    auth_key = "amdpX2dhZzozSzgqZiV6"
    headers = {"Content-Type":"application/json", "Authorization":"Basic " + auth_key}
    request = urllib2.Request(url)
    data = dict()
    for key, value in headers.items():
        request.add_header(key, value)
    try:
        response = urllib2.urlopen(request)
        data = json.loads(response.read())
    except:
        pass
    oid = int()
    if "imgTaxonOid" in data.keys():
        oid = data["imgTaxonOid"]
    return oid

def file_size_check(metadata, min_size):
    '''takes metadata data structure and returns umapped merged count'''
    metadata_outputs = dict()
    all_greater_than_min = True
    for filename in metadata["outputs"]:
        fileloc = filename["file"].split(":")[0]
        metadata_outputs[filename["file"]] = os.stat(os.path.abspath(fileloc)).st_size
        if metadata_outputs[filename["file"]] < min_size:
            all_greater_than_min = False
    return all_greater_than_min

def metadata_config_readme_inputs_consistent(metadata, readme):
    '''takes metadata data structure and read me and ensures that inputs are consistant'''
    metadata_inputs = dict()
    input_count_success = True
    for filename in metadata["inputs"]:
        metadata_inputs[filename] = 1

    readme_inputs = dict()
    with open(readme, "r") as readfile:
        for line in readfile:
            if line.startswith("Filtered"):
                header = re.sub(r'^FilteredData:\s', '', line)
                header = header.rstrip("\n")
                readme_inputs[header] = 1

    if has_missing_keys(metadata_inputs, readme_inputs):
        input_count_success = False
    return input_count_success

def release_spid_in_jamo(metadata):
    '''takes metadata data structure and returns whether spid(s) is alread in jamo or not'''
    released = []
    #get mapping readcount from metadata.json
    spids = []
    spids = metadata['metadata']['sequencing_project_id']
    for spid in spids:
        result = jgi_mga_utils.web_services("jamo", {"metadata.sequencing_project_id":int(spid), "file_type":"scaffolds"})
        jamo_rel_file_count = len(result)
        if jamo_rel_file_count > 0:
            released.append(spid)
    return released

def release_ap_in_jamo(metadata):
    '''takes metadata data structure and returns whether apid is already in jamo or not'''
    released = []
    #get mapping readcount from metadata.json
    apids = []
    apids = metadata['metadata']['analysis_project_id']
    for apid in apids:
        result = jgi_mga_utils.web_services("jamo", {"metadata.analysis_project_id":int(apid), "file_type":"scaffolds"})
        jamo_rel_file_count = len(result)
        if jamo_rel_file_count > 0:
            released.append(apid)
    return released

def fasta_agp_cov_consistent(agp, fasta, cov):
    '''
    Given agp, fasta and coverage files,
    return whether they all have the same count
    '''
    contig_count_success = True
    #agp
    agp_contigs = dict()
    agp_scaffolds = dict()
    with open(agp, "r") as readfile:
        for line in readfile:
            if line.startswith("#"):
                pass
            else:
                fields = line.split()
                try:
                    val = int(fields[-2])
                except ValueError:
                    pass
                if fields[5] >=200:
                    agp_contigs[fields[5]] = 1
                agp_scaffolds[fields[0]] = 1

    #fasta
    fasta_scaffolds = dict()
    with open(fasta, "r") as readfile:
        for line in readfile:
            if line.startswith(">"):
                header = re.sub('^>', '', line)
                header = header.rstrip("\n")
                fasta_scaffolds[header] = 1

    cov_scaffolds = dict()
    with open(cov, "r") as readfile:
        for line in readfile:
            if line.startswith("ID"):
                pass
            elif line.startswith("#"):
                pass
#                print "# in header"
            else:
                fields = line.split()
                scaff = re.sub('_c\d+$', '', fields[0])
                cov_scaffolds[scaff] = 1

    if 0:#debug
        with open("agp.txt",'w') as writefile:
            writefile.write("\n".join(agp_scaffolds) + "\n")
        with open("scafffasta.txt",'w') as writefile:
            writefile.write("\n".join(fasta_scaffolds) + "\n")
        with open("cov.txt",'w') as writefile:
            writefile.write("\n".join(cov_scaffolds) + "\n")
            
    if has_missing_keys(agp_scaffolds, fasta_scaffolds):
        contig_count_success = False
        sys.stdout.write("agp, fasta don't match\n")
    elif has_missing_keys(agp_scaffolds, cov_scaffolds):
        contig_count_success = False
        sys.stdout.write("agp, cov don't match\n")
    return contig_count_success

def has_missing_keys(dict_a, dict_b):
    '''compare two sets and return if they are equal len'''
    has_missing = False
#    if set(dict_a.keys()) == set(dict_b.keys()):
#        has_missing = True
    if len(set(dict_a.keys()) - set(dict_b.keys())) != 0 or  len(set(dict_b.keys()) - set(dict_a.keys())) != 0:
        has_missing = True
    return has_missing

if __name__ == "__main__":
    main()

