#!/usr/bin/env python

'''
jgi_mga_rqc_reporting.py

takes metadata.json and rqc-stats.txt/rqc-files.txt, generates rqc pdf report, 3 sig table files and adds to metadata.json

example:
jgi_mga_rqc_reporting.py -m ../metadata.json -s ../rqc.stats.txt.tmp -f ../rqc.files.txt.tmp -o report.pdf

'''

import sys, os
import argparse
import subprocess
import re
import json
import shutil
import collections

VERSION = "1.0.0"

#RQC_REPORT = "module load jgi-rqc;$JGI_RQC_DIR/report/rqc_report.py "
RQC_REPORT = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/report/rqc_report.py"

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='determine ram given readfile or kmercount.')
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " +  VERSION + ")")
    parser.add_argument("-m", "--metadata",
                        required=True, help="metdata.json\n")
    parser.add_argument("-s", "--rqcstats",
                        required=True, help="rqc.stats.txt file\n")
    parser.add_argument("-f", "--rqcfiles",
                        required=False, help="rqc.files.txt file\n")
    parser.add_argument("-x", "--exclude_rqc_pdf",
                        required=False, default=False, action="store_true", help="exclude rqc pdf (combined)\n")
    parser.add_argument("-o", "--outputdir",
                        required=True, help="output directory\n")
    parser.add_argument("-d", "--debug",
                        required=False, default=False, action="store_true", help="[Optional] print debugging information\n")

    args = parser.parse_args()

    #############1) get library for report generator
    library = ""
    with open(args.rqcstats,"r") as readfile:
        for line in readfile.readlines():
            if line.find("assy.SIG.5.Libraries_Used=") != -1:
                library = re.sub(r"^assy.SIG.5.Libraries_Used=","",line).rstrip()

    ###########detect metatranscriptome
    with open(args.metadata,"r") as readfile:
        metadata_json = json.loads(readfile.read())

    metatranscriptome = False
    for output in metadata_json['outputs']:
        if output['label'].find("etatranscriptome") !=-1:
            metatranscriptome = True

#    if metatranscriptome:
#        args.outputdir = os.path.realpath(args.outputdir + "/..")

    #############2) call report generator
    #/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/report/rqc_report.py -t mg-summary.tex -i $lib -f /global/projectb/sandbox/gaag/bfoster/metaG/viral_metagenome/swift/1114564/rqc-stats.txt,/global/projectb/sandbox/gaag/bfoster/metaG/viral_metagenome/swift/1114564/rqc-files.txt -of rqc-stats.pdf

    if not args.exclude_rqc_pdf:
        if metatranscriptome:
            cmd = RQC_REPORT + " -t mt-summary.tex " 
        else:
            cmd = RQC_REPORT + " -t mg-summary.tex "
            cmd += " -i " + library + " -of " + args.outputdir + "/rqc-stats.pdf" + " -f " + args.rqcstats 
        if args.rqcfiles:
            cmd += "," + args.rqcfiles 
            cmd += " -o " + args.outputdir
        #module load jgi-rqc;$JGI_RQC_DIR/report/rqc_report.py  -t mt-summary.tex -i BZWCA -of rqc-stats.pdf -f ../../rqc-stats.txt -o $PWD ... works
        #module load jgi-rqc;$JGI_RQC_DIR/report/rqc_report.py  -t mg-summary.tex -i BZWCA -f ../rqc-stats.txt,../rqc-files.txt -of rqc-stats.pdf
        if args.debug:
            print cmd
        proc = subprocess.Popen(cmd, shell=True)
        (output, err) = proc.communicate()
        p_status = proc.wait()
        if args.debug:
            if output != None:
                sys.stdout.write("OUT:\n" + output)
            if err != None:
                sys.stdout.write("ERR:\n" + err)

        if p_status == 0:
            pass
        else:
            if output != None:
                sys.stdout.write("OUT:\n" + output)
            if err != None:
                sys.stdout.write("ERR:\n" + err)
            sys.exit(cmd + " exited with error " + str(p_status))

    ############create SIG table text file
    sigs= hasher()
    sigs = create_sig_subset(args.rqcstats)
    sigs['3']['filename'] = 'Table_3_library_information.txt'
    sigs['4']['filename'] = 'Table_4_sequence_processing.txt'
    sigs['5']['filename'] = 'Table_5_metagenome_statistics.txt'

    for table_id in sigs.keys():
        with open(args.outputdir + "/" + sigs[table_id]['filename'], "w") as writefile:
            for line in sigs[table_id]['data']:
                writefile.write(line)


    ###########3) modify metadata.json
    new_metadata = list()
    tmp = dict()
    if not args.exclude_rqc_pdf:
        pdf_destination = args.outputdir + "/rqc-stats.pdf"
        tmp['file'] =  args.outputdir + "/" + "rqc-stats.pdf" + ":" +  os.path.realpath(args.outputdir + "/../" + "rqc-stats.pdf") 
        tmp['label'] =  "rqc_graphical_report"
        new_metadata.append(tmp)

    for table_id in sigs.keys():
        tmp = dict()
        tmp['file']=  args.outputdir + "/" + sigs[table_id]['filename'] + ":" + os.path.realpath(args.outputdir + "/../" + sigs[table_id]['filename'])
        tmp['label'] =  "sigs_" + str(table_id) + "_report"
        new_metadata.append(tmp)
    metadata_json["outputs"] = metadata_json["outputs"] + new_metadata
    shutil.move(args.metadata, args.metadata + ".pre_rqc_pdf")
    with open(args.metadata,"w") as writefile:
        writefile.write(json.dumps(metadata_json,indent=3))

    #update rqc-files.txt
    if args.rqcfiles:
        with open(args.rqcfiles, "a") as myfile:
            if not args.exclude_rqc_pdf:
                myfile.write("rqc_graphical_report=" + os.path.realpath(pdf_destination) + "\n")
            for new in new_metadata:
                myfile.write( new['label'] + "=" + new['file'].split(":")[-1] + "\n")


def create_sig_subset(rqc_stats_file):
    return_dict = hasher()
    sig_lines = []
    with open(rqc_stats_file, "r") as readfile:
        for line in readfile.readlines():
            if line.find("SIG") != -1:
                sig_string = re.sub(r'^.*SIG\.(\d).*$',r'\1',line.rstrip())
                if sig_string not in return_dict:
                    return_dict[sig_string]['data'] = list()
                return_dict[sig_string]['data'].append(line)
    return return_dict

def hasher():
    return collections.defaultdict(hasher)

if __name__ == "__main__":
    main()
