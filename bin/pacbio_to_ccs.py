#!/usr/bin/env python

import argparse
import sys, os
import subprocess
import tempfile
import glob
import re
import shutil

'''
pacbio_to_ccs.py 
takes a list of bamfiles (or bax.h5 ... to be depricated) 
and returns ccs.fasta and non_ccs.fasta

1) translate h5 (if neccessary)
2) generate ccs bam foreach
3) output fastas ccs, and subset output non_ccs

#depends on module load smrtlink/4.0.0.190159,  bax2bam, bam2fasta

Testing datasets ... see log at bottom of script:
module load smrtlink/4.0.0.190159 && /global/u1/b/bfoster/git/jgi-mga/bin/pacbio_to_ccs.py --keep -t ./  /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13679.subreads.bam /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13680.subreads.bam

v 1.0: 20160725
- initial version
'''


VERSION = "1.0.0"
MODULE = "module load smrtlink/4.0.0.190159"

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='generate ccs reads from pacbio data.'
                                     , usage="%(prog)s [options]")

    parser.add_argument('-v', '--version', action='version',
                        version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-p", "--prefix", default="./pacbio", required=False,
                        help="prefix for generated files.\n")
    parser.add_argument("-t", "--tmpdir", required=False,
                        help="tmpdir for intermediate files.\n")
    parser.add_argument("-k", "--keep", default=False, required=False, action='store_true',
                        help="keep tmpdir and copy it dir where output files are placed.\n")
    parser.add_argument("-o", "--overwrite", default=False, required=False, action='store_true',
                        help="force overwrite.\n")
    parser.add_argument('bam', nargs='+', help='bam or bax.h5 files ')

    args = parser.parse_args()


    if not args.tmpdir:
        if os.path.exists(os.environ["TMPDIR"]):
            args.tmpdir = os.environ["TMPDIR"]
        else:
            args.tmpdir = "./"


    os.environ["TMPDIR"] = args.tmpdir
    directory_name = tempfile.mkdtemp()

    if not args.prefix.startswith("/") and not args.prefix.startswith('./'):
        args.prefix = './' + args.prefix

    if not os.path.exists(os.path.dirname(args.prefix)):
        sys.exit(os.path.dirname(args.prefix) + " path does not exist")
    final_ccs_fasta =  args.prefix + ".ccs.fasta"
    final_non_ccs_fasta =  args.prefix + ".non_ccs.fasta"

    if os.path.exists(final_ccs_fasta) and args.overwrite==False:
        sys.exit(final_ccs_fasta + " already exists use --overwrite")
    if os.path.exists(final_non_ccs_fasta) and args.overwrite==False:
        sys.exit(final_non_ccs_fasta + " already exists use --overwrite")

    #bam conversion if needed
    subreads_bam = list()
    for bam in args.bam:
        if bam.endswith(".h5"):
            bamfile = h5_to_bam(bam, directory_name)
        else:
            bamfile = bam
        subreads_bam = list(set(subreads_bam + [bamfile]))
    #bam to ccs subreads_bam should be unique
    ccsfiles_bam = list()
    for bam in subreads_bam:
        ccsfile = bam_to_ccs(bam, directory_name)
        ccsfiles_bam = list(set(ccsfiles_bam + [ccsfile]))

    subreads_fasta = list()
    for bam in subreads_bam:
        fasta = bam_to_fasta(bam, outdir=directory_name)
        subreads_fasta.append(fasta)
        sys.stderr.write("Convertion complete:\n" + fasta + "\n\n")

    ccsfiles_fasta = list()
    for bam in ccsfiles_bam:
        fasta = bam_to_fasta(bam)
        ccsfiles_fasta.append(fasta)
        sys.stderr.write("Convertion complete:\n" + fasta + "\n\n")

    sys.stderr.write("getting ccs ids from:\n" + "\n".join(ccsfiles_fasta) + "\n\n")
    ccs_fasta_ids = list()
    for fasta in ccsfiles_fasta:
        cmd = "cat " + fasta + " >> " + final_ccs_fasta
        lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
        ccs_fasta_ids += fasta_to_ids(fasta,trim_suffix=True)
    ccs_sequence_count = len(ccs_fasta_ids)
    sys.stderr.write("found " + str(ccs_sequence_count) + " records in:\n" + "\n".join(ccsfiles_fasta) +  "\n\n")

    sys.stderr.write("Writing subset of:\n" + "\n".join(subreads_fasta) + "\n\n")
    input_count, write_count, ccs_match_count = write_fasta_subset(subreads_fasta, final_non_ccs_fasta, ccs_fasta_ids)
    sys.stderr.write("Wrote " + str(write_count) + " of " + str(input_count) + " reads to:" + final_non_ccs_fasta + "\n\n")
    sys.stderr.write(str(ccs_match_count) + " reads used to make " +  str(len(ccs_fasta_ids)) + " ccs reads (" + '{0:.{1}f}'.format(float(ccs_match_count) / float(ccs_sequence_count) ,2)  + " average)\n\n")

    if args.keep:
        cp_dest = os.path.dirname(final_ccs_fasta) + "/" +  os.path.basename(directory_name)
        if not os.path.exists(cp_dest):
            cmd = "cp -r " + directory_name + " " + os.path.dirname(final_ccs_fasta)
            lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
            sys.stderr.write("tmpdir " + directory_name + " saved as " + cp_dest + "\n\n")
    else:
        shutil.rmtree(directory_name)


def write_fasta_subset(subreads_fasta_list, outfasta, blacklist_ids):
    '''
    args: subreads_fasta_list = list of fasta files
          outfasta: single output fasta
          blacklist_ids: ids to omit
    returns: input_count, return_count, ccs_match
    '''

    blacklist = dict()
    blacklist = {k: 0 for k in blacklist_ids} 
    size = 80 #fasta line width
    input_count = 0
    return_count = 0
    ccs_match_count = 0
    with open(outfasta, 'w') as writefile:
        for infile in subreads_fasta_list:
            with open(infile, 'r') as fp:
                for name, seq in read_fasta(fp):
                    input_count +=1
                    id_ = re.sub(r'^>*(.*)\/.*$', r'\1', name)
                    if not id_ in blacklist:
                        writefile.write( name + "\n")
                        seqstring = [seq[start:start+size] for start in range(0, len(seq), size)]
                        writefile.write("\n".join(seqstring) + "\n")
                        return_count += 1
                    else:
                        ccs_match_count +=1
    return input_count, return_count, ccs_match_count

def read_fasta(fp):
    '''from biopython'''
    name, seq = None, []
    for line in fp:
        line = line.rstrip()
        if line.startswith(">"):
            if name: yield (name, ''.join(seq))
            name, seq = line, []
        else:
            seq.append(line)
    if name: yield (name, ''.join(seq))
    
def fasta_to_ids(fasta,trim_suffix=False):
    ids = list()
    with open(fasta, 'r') as readfile:
        for line in readfile.readlines():
            line.rstrip()
            if line.startswith(">"):
                if trim_suffix:
                    id_ = re.sub(r'>(.*)\/.*$', r'\1', line.rstrip())
                else:
                    id_ = re.sub(r'>(.*)$', r'\1', line.rstrip())
                ids.append(id_)
    return ids


def bam_to_fasta(bamfile, outdir=""):
    if not outdir:
        outdir = os.path.dirname(bamfile)
    outfile_prefix = outdir + "/" + os.path.basename(bamfile) 
    outfile = outdir + "/" + os.path.basename(bamfile) + ".fasta"

    cmd = MODULE + ";bam2fasta -u -o " + outfile_prefix + " " + bamfile
    sys.stderr.write("Converting bam to fasta for " + bamfile + "\n" + cmd + "\n\n")
    lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
    return outfile

def bam_to_ccs(bamfile,outdir):
    #out.subreads.bam
    #ccs --minLength 100 out.subreads.bam out.subreads.ccs.bam
    outfile = outdir + "/" + os.path.basename(bamfile) + ".ccs.bam"
    cmd = MODULE + ";cd " + outdir + " && ccs --minLength 100 " + bamfile + " " + outfile
    sys.stderr.write("Converting bam to ccs for " + bamfile + "\n" + cmd + "\n\n")
    lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
    return outfile

def h5_to_bam(bamfile,outdir):
    '''
    h5_to_bam()
    args: filename, outdir, prefix
    returns: full path to bamfile
    '''
    if bamfile.endswith(".bam") and os.path.exists(bamfile):
        return bamfile
    if not bamfile.endswith(".h5"):
        sys.stderr.write(bamfile + " not an h5")
        return "na"

    outfile  = outdir + "/" + os.path.basename(bamfile) 
    subreads = re.sub(r'\d+\.bax\.h5', r'bax.h5.subreads.bam', outfile)
    #glob all the h5s 
    glob_txt = re.sub(r'(^.*?)\d+\.bax.h5$',r'\1*.bax.h5',bamfile)
    sys.stderr.write("Gathering all related h5 for:\n" + glob_txt + "\n\n")
    globs = glob.glob(glob_txt)
    sys.stderr.write("Found the following h5s:\n" + "\n".join(globs) + "\n\n")
    if len(globs)>0:
        
        globs.sort(key=os.path.getmtime)
        cmd = MODULE + "; bax2bam " + " ".join(globs) + " -o " + subreads.rstrip(".subreads.bam")
        sys.stderr.write("Converting h5 to bam for:\n" + "\n".join(globs) + "\n\n" + cmd +  "\n\nResult should be:\n" + subreads + "\n\n")
        if not os.path.exists(subreads):
            lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
        else:
            sys.stderr.write(subreads + " already exists, skipping")
    return subreads
    



if __name__ == "__main__":
    main()

'''
command
module load smrtlink/4.0.0.190159 && /global/u1/b/bfoster/git/jgi-mga/bin/pacbio_to_ccs.py --keep -t ./  /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13679.subreads.bam /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13680.subreads.bam
'''

'''
stderr
Converting bam to ccs for /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13679.subreads.bam
module load smrtlink/4.0.0.190159;cd /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz && ccs --minLength 100 /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13679.subreads.bam /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13679.subreads.bam.ccs.bam

>|> 20170727 18:12:23.377 -|- INFO       -|- Polish -|- 0x7fd1100d8700|| -|- negative inf in mutation testing: 'm54017_170526_222611/7340645/29185_34629'
>|> 20170727 18:12:23.772 -|- INFO       -|- Polish -|- 0x7fd1100d8700|| -|- negative inf in mutation testing: 'm54017_170526_222611/7340645/5488_10945'
>|> 20170727 18:13:03.769 -|- INFO       -|- Polish -|- 0x7fd146fc0700|| -|- negative inf in mutation testing: 'm54017_170526_222611/21365103/0_5386'
>|> 20170727 18:13:35.541 -|- INFO       -|- Polish -|- 0x7fd0ba9a8700|| -|- negative inf in mutation testing: 'm54017_170526_222611/31785541/32637_33310'
>|> 20170727 18:13:55.591 -|- INFO       -|- Polish -|- 0x7fd0d3048700|| -|- negative inf in mutation testing: 'm54017_170526_222611/39059671/7087_11513'
>|> 20170727 18:15:32.881 -|- INFO       -|- Polish -|- 0x7fd140e18700|| -|- negative inf in mutation testing: 'm54017_170526_222611/72025052/4750_10335'
Converting bam to ccs for /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13680.subreads.bam
module load smrtlink/4.0.0.190159;cd /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz && ccs --minLength 100 /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13680.subreads.bam /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13680.subreads.bam.ccs.bam

>|> 20170727 18:15:58.504 -|- INFO       -|- Polish -|- 0x7fd3cff76700|| -|- negative inf in mutation testing: 'm54017_170527_082550/25560021/25671_30759'
Converting bam to fasta for /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13679.subreads.bam
module load smrtlink/4.0.0.190159;bam2fasta -u -o /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13679.subreads.bam /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13679.subreads.bam

Convertion complete:
/global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13679.subreads.bam.fasta

Converting bam to fasta for /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13680.subreads.bam
module load smrtlink/4.0.0.190159;bam2fasta -u -o /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13680.subreads.bam /global/dna/dm_archive/sdm/pacbio/00/14/91/pbio-1491.13680.subreads.bam

Convertion complete:
/global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13680.subreads.bam.fasta

Converting bam to fasta for /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13679.subreads.bam.ccs.bam
module load smrtlink/4.0.0.190159;bam2fasta -u -o /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13679.subreads.bam.ccs.bam /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13679.subreads.bam.ccs.bam

Convertion complete:
/global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13679.subreads.bam.ccs.bam.fasta

Converting bam to fasta for /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13680.subreads.bam.ccs.bam
module load smrtlink/4.0.0.190159;bam2fasta -u -o /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13680.subreads.bam.ccs.bam /global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13680.subreads.bam.ccs.bam

Convertion complete:
/global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13680.subreads.bam.ccs.bam.fasta

getting ccs ids from:
/global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13679.subreads.bam.ccs.bam.fasta
/global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13680.subreads.bam.ccs.bam.fasta

found 337 records in:
/global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13679.subreads.bam.ccs.bam.fasta
/global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13680.subreads.bam.ccs.bam.fasta

Writing subset of:
/global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13679.subreads.bam.fasta
/global/projectb/sandbox/gaag/bfoster/fungal_std/bax2bam/example_run/combinatorial/data/_0002/tmphAtjgz/pbio-1491.13680.subreads.bam.fasta

Wrote 629666 of 632738 reads to:./pacbio.non_ccs.fasta

3072 reads used to make 337 ccs reads (9.12 average)

'''
