#!/usr/bin/env python

from __future__ import division
import argparse
import sys
import os
import json
import yaml
import re
import time
import subprocess

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/../lib')
import jgi_mga_utils

VERSION = "1.0.0"

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-d", "--dir", required=True, help="dir.\n")
    parser.add_argument("-c", "--config", required=True, help="yaml.config file.\n")
    parser.add_argument("-i", "--info", required=True, help="info file.\n")
    parser.add_argument("-o", "--output", required=True, help="key value pairs file.\n")

    args = parser.parse_args()
    args.dir = os.path.realpath(args.dir)
    args.dir = args.dir + '/'
    if os.path.exists(args.dir) == 0:
        sys.exit()



    metadata = jgi_mga_utils.hasher()
    with open(args.info, "r") as readfile:
        info = json.loads(readfile.read())
    prefix = str(info["analysis_project_id"])

    filenames = jgi_mga_utils.hasher()
    #for /projectb/sandbox/gaag/bfoster/jgi-mga/bin/jgi_mgaTemplate_seal.template
    filenames["reads_filtered"]["source"] = args.dir + 'bfc/input.corr.fastq.gz'
    filenames["reads_filtered"]["dest"] = args.dir + prefix + '.reads.corr.fastq.gz'

    filenames["metagenome_alignment"]["source"] = args.dir + 'readMappingPairs/pairedMapped.sam.gz'
    filenames["metagenome_alignment"]["dest"] = args.dir + prefix + ".contigs.sam.gz"

    filenames["contigs"]["source"] = args.dir + 'createAGPfile/assembly.contigs.fasta'
    filenames["contigs"]["dest"] = args.dir + prefix + ".contigs.fasta"

    filenames["scaffolds"]["source"] = args.dir + 'createAGPfile/assembly.scaffolds.fasta'
    filenames["scaffolds"]["dest"] = args.dir + prefix + ".scaffolds.fasta"

    filenames["metagenome_assembly_agp"]["source"] = args.dir + 'createAGPfile/assembly.agp'
    filenames["metagenome_assembly_agp"]["dest"] = args.dir + prefix + ".scaffolds.agp"

    filenames["metagenome_assembly_coverage"]["source"] = args.dir + 'readMappingPairs/covstats.txt'
    filenames["metagenome_assembly_coverage"]["dest"] = args.dir + prefix + ".contigs.cov"

    filenames["text_report"]["source"] = args.dir + 'report/report.release.txt'
    filenames["text_report"]["dest"] = args.dir + prefix + ".README.txt"

    filenames["graphical_report"]["source"] = args.dir + 'report/report.release.pdf'
    filenames["graphical_report"]["dest"] = args.dir + prefix + ".README.pdf"

    #### Generate metadata for each output file in metadata.json
    metadata["outputs"] = list()
    tmpdict = dict()
    tmpmetadata = dict()
    for key in filenames.keys():
        tmpdict = {}
        tmpmetadata = {}
        tmpdict["label"] = key
        tmpdict["file"] = filenames[key]["source"] + ":" + filenames[key]["dest"]

        tmpmetadata = get_output_file_metadata(key, filenames, args.config)

        if len(tmpmetadata.keys()) > 0:
            tmpdict["metadata"] = dict(tmpmetadata)
        metadata["outputs"].append(dict(tmpdict))

    #add general email, release to ... etc
    metadata = get_general(metadata)

    #Get the input files actually used in the pipeline from yaml config
    metadata = get_inputs(metadata, args.config)

    #sequecing project AP/AT info and read accounting
    metadata = get_project_metadata(metadata, args.info)
    with open(args.output, "w") as writefile:
        writefile.write(json.dumps(metadata, indent=3))
    return


def get_pipeline_version():
    this_script = sys.argv[0]
    cmd = os.path.dirname(this_script) + '/jgi_mga_meta_rqc.py --version 2>&1'
    version = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).stdout.read().rstrip()
    return version

def detect_assembler(config):
    '''
    Given a config file, determine assembler used
    args: yaml config file
    returns: "megahit", "spades", or "NA"
    '''
    with open(config, "r") as readfile:
        yamldoc = yaml.load(readfile.read())

    if "megahit" in " ".join(yamldoc.keys()):
        return "megahit"
    elif "spades" in " ".join(yamldoc.keys()):
        return "spades"
    else:
        return "NA"

def get_output_file_metadata(key, filepath, config):
    '''
    This routine dispatches metadata generation for an output file  to appropriate routine
    Given keyword, file path and optional config, return designated metadata
    args: keyword, file path, config file
    returns: metadata
    '''
    if key == "contigs" or key == "scaffolds":
        assembler = detect_assembler(config)
        if assembler == "megahit":
            return assembly_metadata_megahit(filepath[key]["source"])
        elif assembler == "spades":
            return(assembly_metadata_spades(filepath[key]["source"]))
        else:
            print "Assembler not found"
            return "NA"
    if key == "reads_filtered":
        return brc_filtered_metadata(filepath[key]["source"])
    if key == "metagenome_assembly_agp":
        return  {}
    if key == "metagenome_assembly_coverage":
        return metagenome_assembly_coverage_metadata(filepath[key]["source"])
    if key == "text_report":
        return {}
    if key == "graphical_report":
        return {}
    if key == "metagenome_alignment":
        return metagenome_alignment_metadata(filepath[key]["source"])

def get_project_metadata(metadata_dot_json, info_config):
    '''
    Given metadata.json and info_config file, return main metadata
    args: metadata.json datastructure, and info_config file
    returns: main metadata section associated with the whole project
    '''
    with open(info_config, "r") as readfile:
        info = json.loads(readfile.read())

    metadata = jgi_mga_utils.hasher()
    date = time.strftime("%Y%m%d")
    metadata["subtitle"] = info["analysis_project_name"] + ", ASSEMBLY_DATE=" + date
    metadata["pipeline_version"] = get_pipeline_version()
    metadata["with_reads"] = False
    metadata["analysis_project_id"] = []
    metadata["analysis_task_id"] = []
    metadata["seq_unit_name"] = []
    metadata["library_name"] = []
    metadata["sequencing_project_id"] = []

    metadata["analysis_project_id"].append(info["analysis_project_id"])
    metadata["analysis_task_id"].append(info["analysis_task_id"])


# - description: input data sequencing platform
#   key: input_platform
#   type: list:string
# - description: input data sequencer instrument used
#   key: input_instrument
#   type: list:string

    filtered_files = [os.path.basename(filename) for filename in metadata_dot_json["inputs"]]
    for proj in info["sequencing_projects"]:
        for lib in proj["lib"]:
            if os.path.basename(lib["filtered"]) in filtered_files:
                metadata["library_name"] = list(set(metadata["library_name"] + [lib["lib"]]))
                metadata["sequencing_project_id"] = list(set(metadata["sequencing_project_id"] + [lib["spid"]]))
                metadata["seq_unit_name"] = list(set(metadata["seq_unit_name"] + [lib["sequnit"]]))

    if len(set(metadata["library_name"])) == 1:#all libs the same
        metadata["library_name"] = list(set(metadata["library_name"]))
    if len(set(metadata["sequencing_project_id"])) == 1:#all libs the same
        metadata["sequencing_project_id"] = list(set(metadata["sequencing_project_id"]))

    metadata.update(get_read_accounting(metadata_dot_json, info))
    metadata_dot_json["metadata"] = metadata
    return metadata_dot_json

def get_read_accounting(metadata_json, info_config):
    '''
    Gets raw read count, reads filtered out, read corrector removed, and final read set count
    args: dict of filenames
    returns: dictionary with raw, filtered, final_read_count
    '''
    read_accounting = dict()
    #Get raw and filterd read count
    raw_count = 0
    filtered_count = 0
    final_read_count_aln = 0
    final_read_count_spades = 0
    final_read_count_split = 0

    filtered_files = [os.path.basename(filename) for filename in metadata_json["inputs"]]
    for proj in info_config["sequencing_projects"]:
        for lib in proj["lib"]:
            if os.path.basename(lib["filtered"]) in filtered_files:
                raw_count = raw_count + int(lib["file_read_count"])
                raw_bp = raw_count + int(lib["file_base_count"])
                filtered_count = filtered_count + lib["filtered_read_count"]

    #reads and bases from split log
    cmd = 'grep "^Input:" ' + os.path.dirname(metadata_json["outputs"][0]["file"].split(":")[0]) + "/../split/command.bash.e"

    line = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()[0].rstrip()
    final_read_count_split = int(re.sub(r'^.*\b(\d+)\sread.*$', r'\1', line))
    final_read_bp_split = int(re.sub(r'^.*\b(\d+)\sbases.*$', r'\1', line))

    for output in metadata_json["outputs"]:
        if output["label"] == "metagenome_alignment":
            final_read_count_aln = output["metadata"]["num_input_reads"]

    #double check with reads from spades
#
#    cmd = "grep -m 1 Processed " +  os.path.dirname(metadata_json["outputs"][0]["file"].split(":")[0]) + "/../spades/spades3/spades.log"
#    line = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()[0].rstrip()
#    0:00:17.388    17G / 17G   INFO    General                 (kmer_splitters.hpp        : 153)   Processed 11872372 reads
#    final_read_count_spades = re.sub(r'^.*Processed\s(\d+)\sreads.*$', r'\1', line)
#    final_read_count_spades = int(int(final_read_count_spades)/2)


    cmd = 'grep -m 1 "reads written" ' +  os.path.dirname(metadata_json["outputs"][0]["file"].split(":")[0]) + "/../spades/spades3/spades.log"
    line = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()[0].rstrip()
    #  0:01:43.687   332M / 348M  INFO    General                 (binary_converter.hpp      : 159)   22348674 reads written
    final_read_count_spades = re.sub(r'^.*\s(\d+)\sreads\swritten.*$', r'\1', line)
    final_read_count_spades = int(int(final_read_count_spades)*2)

#    if final_read_count_aln != final_read_count_split or final_read_count_aln != final_read_count_spades:
#        print "final_readcount_aln=" + str(final_read_count_aln)
#        print "final_readcount_split=" + str(final_read_count_split)
#        print "final_readcount_spades=" + str(final_read_count_spades)
#        sys.exit("spades, alignment, and split read counts don't match")

#/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/7/1125622/readlen/readlen.txt
    read_len_file = os.path.dirname(metadata_json["outputs"][0]["file"].split(":")[0]) + "/../readlen/readlen.txt"
    read_len = dict()
    with open(read_len_file, "r") as readfile:
        for line in readfile.readlines():
            if line.startswith('#') and len(line.split()) == 2:
                read_len[line.split()[0].lstrip('#')] = line.split()[-1]
    
    rename = {"Max:": "input_len_max_bp", "Min:": "input_len_min_bp", "Mode:": "input_len_mode", "Median:": "input_len_median", "Std_Dev:": "input_len_stdev",  "Avg:": "input_len_avg_bp", "Reads:": "input_reads_n", "Bases:": "input_bases_bp"}
    for old, new in rename.iteritems():
        read_len[new] = [float(read_len.pop(old))]
    read_len['raw_reads_n'] = [float(raw_count)]
    read_len['raw_reads_bp'] = [float(raw_bp)]
    read_len['filt_reads_n'] = [float(filtered_count)]
    return read_len


def get_inputs(metadata_dot_json, config):
    '''
    Given config file, and metadata.json data structure returns modified structure with inputs added
    args: metadata.json data structure, yaml config file
    returns:  modified structure with inputs added
    '''
    files = []
    with open(config, "r") as readfile:
        yamlconfig = yaml.load(readfile.read())
    files = yamlconfig['var']['__input'].split(" ")
    metadata_dot_json["inputs"] = files
    return metadata_dot_json

def get_general(metadata_dot_json):
    '''
    Given metadata.json data structure, add default vals for send_email and release_to
    args: metadata.json data structure
    returns: modified  metadata.json data structure with values for send_email and release_to added
    '''
    metadata_dot_json["send_email"] = True
    metadata_dot_json["release_to"] = ["portal", "img"]
    return metadata_dot_json

####
# The following section begins routines to fetch metadata for each output file in metadata.json
##########################################################

def brc_filtered_metadata(files):
    '''
    Given bfc file, get input, output and percent filtered counts
    in metadata entry for filtered file
    '''
    tmpmetadata = dict()
    tmpmetadata['corrector'] = "bfc"
    cmd = "cat " + os.path.dirname(files) + "/bfc.version"
    tmpmetadata['corrector_version'] = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()[0].rstrip()

    with open(os.path.dirname(files) + "/command.bash", "r") as readfile:
        for line in readfile.readlines():
            line = line.rstrip().lstrip()
            if line.startswith("bfc"):
                tmpmetadata["corrector_parameters"] = re.sub(r'^bfc\s(.*?)\s\/.*$', r'\1', line)

    #TODO add read cound data from readlen call
    cmd = "zcat " + os.path.dirname(files) + "/input.fastq.gz | wc -l "
    input = int(subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()[0].rstrip())/4
    cmd = "zcat " + os.path.dirname(files) + "/input.corr.fastq.gz | wc -l "
    output = int(subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()[0].rstrip())/4
    tmpmetadata['num_input_reads'] = int(input)
    tmpmetadata['contam_filtered'] = int(input - output)
    tmpmetadata['perc_artifact'] = "%.1f" % (float(tmpmetadata['contam_filtered'])/float(tmpmetadata['num_input_reads']) * 100)
    tmpmetadata['perc_artifact'] = float(tmpmetadata['perc_artifact'])
    return tmpmetadata

def metagenome_assembly_coverage_metadata(files):
    '''
    Given files array , return assembly coverage metadata record
    args:cov file path
    returns: metadata record with merger_version, merger, readcount_input, avg_insert
    '''
    tmpmetadata = dict()
    count = 0
    with open(files) as readfile:
        for line in readfile.readlines():
            if line.startswith('#') or line.startswith('ID'):
                continue
            else:
                count = count + 1
    tmpmetadata['sequence_count'] = count
    return tmpmetadata

def assembly_metadata_spades(files):
    '''
    Given spades file, return metadata record
    args: spades file
    returns: assembler, assembler_version, assembler_parameters,
    '''
    tmpmetadata = dict()
    tmpmetadata['assembler'] = ""
    tmpmetadata['assembler_version'] = ""
    tmpmetadata['assembler_parameters'] = ""
    stdout = jgi_mga_utils.get_glob(os.path.dirname(files) + "/../spades/spades3/spades.log", latest=True)
    tmpmetadata['assembler'] = "spades"

    with open(stdout, "r") as readout:
        for line in readout.readlines():
            if line.startswith("Command line:") and tmpmetadata['assembler_parameters'] == "":
                tmpmetadata['assembler_parameters'] = re.sub(r'^Command line: ', r'', line)
            if line.find("SPAdes version:") != -1:
                tmpmetadata['assembler_version'] = line.lstrip().rstrip()
    #cleanup command assembler parameters
    tmpmetadata['assembler_parameters'] = re.sub(r'\S+spades.py', r'', tmpmetadata['assembler_parameters'])
    tmpmetadata['assembler_parameters'] = re.sub(r'--tmp-dir\t\S+', r'', tmpmetadata['assembler_parameters'])
    spades_outputs = [r"-o", r"--12", r"-1", r"-2", r"-s", r"--pe\S+-12", r"--pe\S+-1", r"--pe\S+-2", r"--pe\S+-s", r"--pe\S+-<or>", r"--s\S+", r"--mp\S+-12", r"--mp\S+-1", r"--mp\S+-2", r"--mp\S+-s", r"--mp\S+-<or>", r"--hqmp\S+-12", r"--hqmp\S+-1", r"--hqmp\S+-2", r"--hqmp\S+-s", r"--hqmp\S+-<or>", r"--nxmate\S+-1", r"--nxmate\S+-2", r"--sanger", r"--pacbio", r"--nanopore", r"--tslr", r"--trusted-contigs", r"--untrusted-contigs"]
    for output in spades_outputs:
        tmpmetadata['assembler_parameters'] = re.sub(r'\t' + output + r'\t\S+', r'', tmpmetadata['assembler_parameters'])
    tmpmetadata['assembler_parameters'] = re.sub(r'\t', r' ', tmpmetadata['assembler_parameters'])
    tmpmetadata['assembler_parameters'] = tmpmetadata['assembler_parameters'].rstrip().lstrip()

    #/usr/common/jgi/assemblers/spades/3.10.1/bin/spades.py|--tmp-dir|/scratch/tmp/34235320.1.normal_excl.q|-m|2000|-o|/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/2/1125622/spades/spades3|--only-assembler|-k|127|--meta|-t|32|-1|/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/2/1125622/split/reads1.fasta|-2|/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/2/1125622/split/reads2.fasta|

    cmd = "cat " + jgi_mga_utils.get_glob(os.path.dirname(files)) + "/../assembly_stats/assembly.scaffolds.fasta.stats.tsv"
    stats = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
    headers = stats[0].split()
    data = stats[1].split()
    if len(headers) != len(data):
        sys.exit("stats header count != datacount")

    for i in range(0, len(headers)):
        headers[i] = re.sub('^#', '', headers[i])
        if not float(data[i]).is_integer() or float(data[i]) == 0:
            tmpmetadata[headers[i]] = float(data[i])
        else:
            tmpmetadata[headers[i]] = long(data[i])
    return tmpmetadata


def assembly_metadata_megahit(files):
    '''
    Given megahit file, return metadata record
    args: megahit file
    returns: assembler, assembler_version, assembler_parameters,
    '''
    tmpmetadata = dict()
    tmpmetadata['assembler'] = ""
    tmpmetadata['assembler_version'] = ""
    tmpmetadata['assembler_parameters'] = ""
    stdout = jgi_mga_utils.get_glob(os.path.dirname(files) + "/../megahit/megahit_out/log", latest=True)

    tmpmetadata['assembler'] = "MEGAHIT"
    cmd = 'grep "MEGAHIT v" ' + stdout
    tmpmetadata['assembler_version'] = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()[0].rstrip()
    opts = jgi_mga_utils.get_glob(os.path.dirname(files) + "/../megahit/megahit_out/opts.txt")
    with open(opts, "r") as readfile:
        for line in readfile.readlines():
            tmpmetadata['assembler_parameters'] += " " + line.rstrip()

    cmd = "cat " + jgi_mga_utils.get_glob(os.path.dirname(files)) + "/../assembly_stats/assembly.scaffolds.fasta.stats.tsv"
    stats = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
    headers = stats[0].split()
    data = stats[1].split()
    if len(headers) != len(data):
        sys.exit("stats header count != datacount")

    for i in range(0, len(headers)):
        headers[i] = re.sub('^#', '', headers[i])
        if not float(data[i]).is_integer() or float(data[i]) == 0:
            tmpmetadata[headers[i]] = float(data[i])
        else:
            tmpmetadata[headers[i]] = long(data[i])
    return tmpmetadata

def reads_rqcfiltered_metadata(files):
    '''
    Given rqc filtered file, returns associated metadata record
    args: rqcfiltered file
    returns: num_input_reads, contam_filtered, perc_artifact,
    '''
    files = os.path.basename(files)
    tmpmetadata = dict()
    tmpmetadata["num_input_reads"] = 0
    tmpmetadata["contam_filtered"] = 0
    tmpmetadata["perc_artifact"] = 0

    #filtered metadata
    metadata = jgi_mga_utils.web_services('jamo', {'file_name':files})[0]
    if "raw_reads_count" in metadata["metadata"].keys():
        tmpmetadata["num_input_reads"] = metadata["metadata"]["raw_reads_count"]
        tmpmetadata["contam_filtered"] = metadata["metadata"]["raw_reads_count"] - metadata["metadata"]["filter_reads_count"]
    elif "filter_reads_count" in metadata["metadata"].keys():
        #.metadata.filter_reads_count,.metadata.input_read_count,.metadata.filter_reads_count_pct'
        tmpmetadata["num_input_reads"] = metadata["metadata"]["input_read_count"]
        tmpmetadata["contam_filtered"] = tmpmetadata["num_input_reads"] - metadata["metadata"]["filter_reads_count"]
#    tmpmetadata['perc_artifact'] = "%.1f" % (tmpmetadata['contam_filtered']/tmpmetadata['num_input_reads'] * 100)
#    tmpmetadata["perc_artifact"] = float(tmpmetadata["perc_artifact"])
    return tmpmetadata

def metagenome_alignment_metadata(files):
    '''
    Given alignment file,  return metadata
    args: alignment file
    returns: aligner, aligner_version, aligner_parameters
    '''
    tmpmetadata = dict()


#@PG     ID:BBMap        PN:BBMap        VN:37.32        CL:java -Djava.library.path=/usr/common/jgi/utilities/bbtools/prod-v37.32/lib -ea -Xmx425599m align2.BBMap build=1 overwrite=true fastareadlen=500 interleaved=true ambiguous=random in=/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/1125622/bfc/input.corr.fastq.gz ref=/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/1125622/createAGPfile/assembly.scaffolds.fasta out=/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/1125622/readMappingPairs/pairedMapped.sam.gz covstats=/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/1125622/readMappingPairs/covstats.txt

    cmd = "gzip -cd " + files + " 2> /dev/null| grep -m 1 \"@PG\""
#    cmd = "module load samtools;gzip -cd  " + files + " 2>/dev/null | samtools view -SH - 2>/dev/null | tail -1"
    lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()

    if len(lines) != 1:
        sys.exit("header version file not correct" + str(files))

    tmp = dict(re.findall(r'(\S+):(\S+)', lines[0]))
    tmpmetadata["aligner"] = tmp["ID"]
    tmpmetadata["aligner_version"] = tmp["VN"]
    tmpmetadata["aligner_version"] = tmp["VN"]
    tmpmetadata["aligner_parameters"] = re.sub(r'^.*CL:(.*)$', r'\1', lines[0]).rstrip()
    #aligner_parameters
    stderr = jgi_mga_utils.get_glob(os.path.dirname(files) + "/command.bash")
    cmd = "grep bbmap " + stderr
    lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
    if len(lines) != 1:
        sys.exit("header version file not correct" + str(stderr))


    #alignment stats
    num_aligned_reads = ""
    bbmapfile = os.path.dirname(files) + '/command.bash.e'
    cmd = "grep \"^mapped:\" " + bbmapfile + r' |  cut -f 3 |  sed "s/\s//g"'
    lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
    if len(lines) != 2:
        sys.exit("alignment stats file not correct" + str(cmd))

    num_aligned_reads = int(lines[0]) + int(lines[1])
#    result = re.search(r'^(\d+).*$', lines[0])
#    if hasattr(result,'group'):
#        if result.group(1):
#            num_aligned_reads = result.group(1)
    tmpmetadata["num_aligned_reads"] = int(num_aligned_reads)

    num_input_reads = ""
    cmd = "grep \"^Reads Used:\" " + bbmapfile + " | cut -f 2"
    lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
    if len(lines) != 1:
        sys.exit("alignment stats file not correct" + str(cmd))
    result = re.search(r'^(\d+).*$', lines[0])
    if hasattr(result, 'group'):
        if result.group(1):
            num_input_reads = result.group(1)
    tmpmetadata["num_input_reads"] = int(num_input_reads)
    return tmpmetadata
################ End metadata generation routines ##################

if __name__ == "__main__":
    main()


