#!/usr/bin/env python

import sys, os
import calendar, time
import json
import argparse
import re
import textwrap

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/../lib')
import jgi_mga_utils
import qcutils

#todo
#1) meta transcriptome attach

VERSION='1.0.0'

METAGENOME_EMAIL = 'Auto-Email_MetaG_MetaT-10-17-2017.doc.txt.html'
METAGENOME_EMAIL = os.path.dirname(os.path.realpath(__file__)) + '/' + METAGENOME_EMAIL

FUNGAL_EMAIL = 'Auto-Email_MetaG_Fungal_MetaT-10-23-2017.doc.txt.html'
FUNGAL_EMAIL =  os.path.dirname(os.path.realpath(__file__)) + '/' + FUNGAL_EMAIL


def main():
    '''main'''
    parser = argparse.ArgumentParser(description='email assembly results.')
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " +  VERSION + ")")
    parser.add_argument("-x", "--execute",
                        required=False, default=False, action='store_true', help="[Optional] execute \n")
    parser.add_argument("-b", "--body",
                        required=False, help="email body file. default autodetect\n")
    parser.add_argument("-m", "--metadata",
                        required=True, help="metadata.json file\n")
    parser.add_argument("-t", "--tmpdir",
                        required=False, default=os.getcwd(), help="metadata.json file\n")
    parser.add_argument("-o", "--verbose",
                        required=False, default=False, help="verbose\n")

    args = parser.parse_args()

    with open(args.metadata, "r") as f:
        md = json.loads(f.read())
    
    spid = md['metadata']['sequencing_project_id'][0]
    apid = md['metadata']['analysis_project_id'][0]
    su = md['metadata']['seq_unit_name'][0]
    

    dump = jgi_mga_utils.web_services('rqc_dump_by_su', su)
    fungal=False
    metatranscriptome=False
    if not args.body:
        if 'Fungal' in dump['library_info']['account_jgi_sci_prog']:
            fungal=True
            args.body = FUNGAL_EMAIL
            if 'Metatranscriptome' in dump['library_info']['actual_seq_prod_name']:
                metatranscriptome=True
                subtitle = "Fungal Metatranscriptome: " + str(spid) + " - " + dump['library_info']['seq_proj_name']
            else:
                subtitle = 'Fungal Metagenome: ' + str(spid) + ' - ' + md['metadata']['subtitle']
        elif 'Metatranscriptome' in dump['library_info']['actual_seq_prod_name']:
            args.body = METAGENOME_EMAIL
            metatranscriptome=True
            subtitle = "Metagenome Metatranscriptome: " + str(spid) + " - " + dump['library_info']['seq_proj_name']
        elif 'Metagenome' in dump['library_info']['actual_seq_prod_name'] or 'Cell Enrichment' in dump['library_info']['actual_seq_prod_name']:
            args.body = METAGENOME_EMAIL
            if 'subtitle' in md['metadata'].keys():
                subtitle = 'Metagenome: ' + str(spid) + ' - ' + md['metadata']['subtitle']
            else:
                subtitle = "Metagenome: " + str(spid) + " - " + dump['library_info']['actual_seq_proj_name']
        else:
            print dump['library_info']['actual_seq_prod_name'] + " Not supported"
            sys.exit()

    pdf_file_name = ""
    for output in md['outputs']:
        if output['label'] == "rqc_graphical_report":
            if metatranscriptome:
                pdf_file_name = os.path.dirname(os.path.realpath(args.metadata)) + "/../" + os.path.basename(output['file'])
            else:
                pdf_file_name = os.path.dirname(os.path.realpath(args.metadata)) + "/" + os.path.basename(output['file'])
        elif os.path.exists(os.path.dirname(os.path.realpath(args.metadata)) + "/../rqc-stats.pdf"):
            pdf_file_name = os.path.dirname(os.path.realpath(args.metadata)) + "/../rqc-stats.pdf"
        elif os.path.exists(os.path.dirname(os.path.realpath(args.metadata)) + "/rqc-stats.pdf"):
            pdf_file_name = os.path.dirname(os.path.realpath(args.metadata)) + "/rqc-stats.pdf"

    proposal_id, proposal_title, pi_name, pi_email = qcutils.get_proposal_info(spid, verbose=args.verbose)
    pi_last_name, pi_first_name = pi_name.split(", ")
    pm_name, pm_email  = qcutils.get_project_manager(spid, verbose=args.verbose)
    pm_last_name, pm_first_name = pm_name.split(", ")
    tmpfile = args.tmpdir + "/tmpmail" + str(calendar.timegm(time.gmtime())) + ".txt.html"
    
    ap = qcutils.get_analysis_project(apid, verbose=args.verbose)
    fd =  ap['final_deliv_project_id']
    string_list = 'pi_last_name pi_first_name pi_email pm_last_name pm_first_name pm_email'
    string_list = tuple(string_list.split())
    subst = {'{{' + value + '}}': value for value in string_list}
    subst["{{JGI_genome_portal_page.}}"]='<a href="https://genome.jgi.doe.gov/lookup?keyName=jgiProjectId&keyValue=' + str(fd)  + '&app=Info">JGI genome portal page.</a>'
    subst["{{IMG/M}}"]='<a href="https://img.jgi.doe.gov/">IMG/M</a>'
    subst["{{SRA}}"]='<a href="https://www.ncbi.nlm.nih.gov/sra">SRA</a>'
    subst["{{Standards_in_Genomic_Sciences}}"]='<a href="https://standardsingenomics.biomedcentral.com/submission-guidelines/preparing-your-manuscript/metagenome-report">Standards in Genomic Sciences</a>'
    subst["{{Microbiome}}"]='<a href="https://microbiomejournal.biomedcentral.com/submission-guidelines/preparing-your-manuscript/microbiome-announcement">Microbiome</a>'
    subst["{{Scientific_Data}}"]='<a href="https://www.nature.com/sdata/publish/for-authors">Scientific Data</a>'


    with open(args.body, "r") as readfile:
        with open(tmpfile, "w") as writefile:
            writefile.write("<html>\n<body>\n")
            for line in readfile.readlines():
                if re.match(r'^\n',line):
                    line = re.sub(r'^\n','<br/>',line)
                elif len(line) > 50: 
                    line = "<br/>".join(textwrap.wrap(line, width=80)) + "<br/>" 
                else:
                    line = line + "<br/>"
                for key in subst.keys():
                    if "href" in subst[key]:
                        line = re.sub(key, subst[key], line)
                    else:
                        line = re.sub(key, eval(subst[key]), line)
                writefile.write(line)
            writefile.write("\n</body>\n</html>\n")

            
    (analyst_fullname, analyst_email) = qcutils.get_analyst_info(verbose=args.verbose)
    from_email = "%s <%s>" % (analyst_fullname, analyst_email)
    bcc = ' -b bfoster@lbl.gov'
    if not args.execute:
        recipient = 'bfoster@lbl.gov'
    else:
        recipient = pi_email + ',' + pm_email + ',' + 'bfoster@lbl.gov'
        recipient += ',accopeland@lbl.gov,aclum@lbl.gov,eaeloefadrosh@lbl.gov'
#    reply_to = "Alex Copeland <accopeland@lbl.gov>, Alicia Clum <aclum@lbl.gov>, Brian Foster <bfoster@lbl.gov>, Emiley Eloe-Fadrosh <eaeloefadrosh@lbl.gov>"
    reply_to = "Brian Foster <bfoster@lbl.gov>, Emiley Eloe-Fadrosh <eaeloefadrosh@lbl.gov>, " + pm_email
    pdf_file_name = " -a " + pdf_file_name 
    email_cmd = "sleep 3; cat %s | EMAIL=\"%s\" REPLYTO=\"%s\" mutt -e \"set content_type=text/html\" %s -s \"%s\" %s -- %s" % (tmpfile, from_email, reply_to, bcc, subtitle, pdf_file_name, recipient)
    time.sleep(2)
    print email_cmd

    sys.exit()

def get_program(metadata):
    return
if __name__ == "__main__":
    main()


# get all the project information we need
#sequencing_project_name = qcutils.get_proj_info(sequencing_project_id, verbose=args.verbose)[0][2]
#proposal_id, proposal_title, pi_name, pi_email = qcutils.get_proposal_info(sequencing_project_id, verbose=args.verbose)


#project_info = { 'sequencing_project_id'     : [sequencing_project_id],
#                 'sequencing_project_name'   : sequencing_project_name,
#                 'seq_unit_name'             : [seq_unit_name,], 
#                 'library_name'              : [library_name],
#                 'proposal_title'            : proposal_title,
#                 'proposal_id'               : proposal_id,
#                 'pi_name'                   : pi_name,
#                 'pi_email'                  : pi_email }

#analysis_projects = qcutils.get_analysis_projects(sequencing_project_id,verbose=args.verbose)
#assembly_ap, mapping_self_ap, mapping_other_aps = jgi_mt.separate_aps(analysis_projects)

#BEGIN create reports and release information


#if args.email:
#    recipient = args.email
#    (analyst_fullname, analyst_email) = qcutils.get_analyst_info(verbose=args.verbose)
#    from_email = "%s <%s>" % (analyst_fullname, analyst_email)
#    subject = "Metatranscriptome - %d - %s" % (sequencing_project_id, sequencing_project_name)
#    reply_to = "Alex Copeland <accopeland@lbl.gov>, Alicia Clum <aclum@lbl.gov>, Brian Foster <bfoster@lbl.gov>, Adam Rivers <arrivers@lbl.gov>"

#    sys.stderr.write("Emailing report to %s\n" % recipient)
#    if args.verbose:
#        sys.stderr.write("%s\n" % email_cmd)
#    proc = subprocess.check_call(email_cmd,shell=True)
