#!/usr/bin/env python
'''
jgi_mga_create_pipeline_yaml.py

'''
import sys
import os
import argparse
import collections
import json

VERSION = "1.0.0"

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Metagenome rqc pipelile yaml generator.', usage="%(prog)s -i <input json file>  -t [template file]")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-i", "--input", required=True, help="input json file\n")
    parser.add_argument("-t", "--template", required=True, help="template yaml file\n")

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-a", "--all", default=False, action='store_true', help="all filtered files\n")
    group.add_argument("-x", "--exclude_usable_false", default=False, action='store_true', help="all excluding usable false\n")

    args = parser.parse_args()

    template = ""
    args.input = os.path.abspath(args.input)
    if args.template:
        with open(args.template, "r") as readfile:
            template = readfile.read()
    else:
        sys.exit("template required")
    with open(args.input, "r") as readfile:
        jsoninput = json.loads(readfile.read())

    substitutions = hasher()
    substitutions["__INFO__"] = args.input
    substitutions["__BIN_DIR__"] = os.path.abspath(os.path.dirname(sys.argv[0]))
    if args.all:
        substitutions["__INPUT_FILES__"] = " ".join(parse_input_files(jsoninput, mode="all"))
    elif args.exclude_usable_false:
        substitutions["__INPUT_FILES__"] = " ".join(parse_input_files(jsoninput, mode="exclude_usable_false"))
    else:
        sys.exit("no mode for selecting input files, select -a or -x")

    with open(args.template, "r") as readfile:
        template = readfile.read()

    template = substitute_vars(substitutions, template)

    with open(args.input + ".config.yaml", "w") as writefile:
        writefile.write(template)

def substitute_vars(variables, template):
    '''
    Substitutes variables in the template
    args:
        vars: dict of key value to substitute in the template
        template: text string containing template to be substituted
    returns:
        template with all the substitutions complete
    '''
    temp = ""
    for line in template.split("\n"):
        for key in variables.keys():
            line = line.replace(key, variables[key])
        temp = temp + line + "\n"
    return temp

def parse_input_files(info, mode="exclude_usable_false"):
    '''
    Given input file, return input files
    args:
       info: file json with filtered files
       mode: [enforce_single, exclude_usable_false, all]
    returns:files
    filtered filesfiles = []
    '''
    valid_modes = ["exclude_usable_false", "all"]
    if mode not in valid_modes:
        sys.exit(mode + " not in " + ",".join(valid_modes))
    files = []
    for seq in info["sequencing_projects"]:
        for lib in seq["lib"]:
            filt = lib.get("filtered")
            usable = lib.get("usable")
            if mode == "exclude_usable_false" and usable != False:
                files.append(filt)
            elif mode == "all":
                files.append(filt)
            else:
                sys.exit("mode " + mode + " not recognized")
    for filename in files:
        if not os.path.isfile(filename):
            sys.stderr.write("WARNING can't find " + filename)
            sys.exit()
    return files

def hasher():
    '''allows assigning arbitrary depth hash'''
    return collections.defaultdict(hasher)

if __name__ == "__main__":
    main()

