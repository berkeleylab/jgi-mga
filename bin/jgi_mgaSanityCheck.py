#!/usr/bin/env python

from __future__ import division
import argparse
import sys
import os
import subprocess
import glob
import json
import collections
import re
import time
import datetime

sys.path.insert(0,os.path.dirname(os.path.realpath(__file__))+'/../lib')
import jgi_mgaUtils

version = "0.0.1"

def main():
    sanityCheckSuccess = True
    parser = argparse.ArgumentParser(description='Description.')
    
    parser.add_argument('-v','--version', action='version',version="%(prog)s (version " +  version + ")")

    parser.add_argument("-d","--dir",required=True,help="dir.\n")
    parser.add_argument("-e","--email",required=False,help="email address to send pdf.\n")

    args = parser.parse_args()
    args.dir = os.path.realpath(args.dir)
    if (os.path.exists(args.dir) == 0):
        sys.exit()

    if (args.email and not '@lbl.gov' in args.email):
        print "email address " + args.email + " looks wrong\n"
        sys.exit()

    test = "checking metadata.json..."
    sys.stdout.write(test)
    met = get_glob(args.dir + "/metadata.json")
    if (metadata_json_check(met)==True):
        sys.stdout.write("\t\t\t\tOK\n")
    else:
        sys.stdout.write("\t\t\t\tFAIL\n")
        sanityCheckSuccess = False
        

    test = "checking ctg cnt:agp,fa,cov..."
    sys.stdout.write(test)
    agp = get_glob(args.dir + "/*.agp")
    fasta = get_glob(args.dir + "/*.fasta")
    cov = get_glob(args.dir + "/*.cov")
    if (contig_count(agp,fasta,cov)==True):
        sys.stdout.write("\t\t\t\tOK\n")
    else:
        sys.stdout.write("\t\t\t\tFAIL\n")
        sanityCheckSuccess = False

    
#    test = "checking mapping counts..."
#    sys.stdout.write(test)
#    if (check_mapping(args.dir)==True):
#        sys.stdout.write("\t\t\t\tOK\n")
#    else:
#        sys.stdout.write("\t\t\t\tFAIL\n")
#        sanityCheckSuccess = False        
    

    sys.stdout.write("checking filesizes of links...")
    if (check_release_files(args.dir)==True):
        sys.stdout.write("\t\t\t\tOK\n")
    else:
        sys.stdout.write("\t\t\t\tFAIL\n")
        sanityCheckSuccess = False        

    sys.stdout.write("checking if prev release...")
    if (first_release(args.dir)==True):
        sys.stdout.write("\t\t\t\tOK\n")
    else:
        sys.stdout.write("\t\t\t\tWARNING\n")
#        sanityCheckSuccess = False        

    sys.stdout.write("checking ap id ...")
    if (new_ap_id(args.dir)==True):
        sys.stdout.write("\t\t\t\tOK\n")
    else:
        sys.stdout.write("\t\t\t\tFAIL\n")
        sanityCheckSuccess = False        


    sys.stdout.write("checking merged files...")
    if(check_mergedII(args.dir)==True):
        sys.stdout.write("\t\t\t\tOK\n")
    else:
        sys.stdout.write("\t\t\t\tWARNING\n")
#        sanityCheckSuccess = False        




    if (args.email):
        sys.stdout.write("emailing pdf...")
        if (email_pdf(args.dir,'bfoster@lbl.gov')==True):
            sys.stdout.write("\t\t\t\tOK\n")
        else:
            sys.stdout.write("\t\t\t\tFAIL\n")
            sanityCheckSuccess = False

    if (sanityCheckSuccess == False):
        sys.exit(1)
    else:
        sys.exit(0)

def check_mergedII(dir):
    merged_success = True
    met = get_glob(dir + "/metadata.json")
    f = open(met,"r")
    pObj = json.loads(f.read())
    f.close()

    #getting unmapped reads
    unmapped_merged_input = int()
    unmapped_merged_output = int()
    for output in pObj['outputs']:
        if output['label'] == "metagenome_unmapped_merged":
            unmapped_merged_input = output['metadata']['readcount_input'] * 2
            unmapped_merged_output = output['metadata']['readcount'] * 2

    unmapped_unmerged_input= int()
    unmapped_unmerged_output= int()
    for output in pObj['outputs']:
        if output['label'] == "metagenome_unmapped_unmerged":
            unmapped_unmerged_input = output['metadata']['readcount_input'] * 2
            unmapped_unmerged_output = output['metadata']['readcount'] * 2

    if (unmapped_unmerged_input != unmapped_merged_input):
        merged_success = False

    #<  20% merge rate for unaligned reads
    if (unmapped_merged_input/unmapped_merged_output < 0.2):
        pass
        merged_success = False
    #unmerged and merged add up to input
    if (((unmapped_merged_output + unmapped_unmerged_output)- unmapped_merged_input) !=0):
        merged_success = False

    if merged_success == False:
         sys.stdout.write(dir + "\n")
         sys.stdout.write("unmapped_merged_input: " + str(unmapped_merged_input)+"\n")
         sys.stdout.write("unmapped_merged_output: " + str(unmapped_merged_output)+"\n")
         sys.stdout.write("unmapped_unmerged_input: " + str(unmapped_unmerged_input)+"\n")
         sys.stdout.write("unmapped_unmerged_output: " + str(unmapped_unmerged_output)+"\n")
         sys.stdout.write("unmapped_merged_input/unmapped_merged_output: " + str(unmapped_merged_input/unmapped_merged_output)+"\n")

#         sys.stdout.write("unmapped merged " + str(unmapped_merged)+"\n")
#         sys.stdout.write("unmapped unmerged " + str(unmapped_unmerged)+"\n") 
#         sys.stdout.write("unmapped output - input " + str(((unmapped_merged + unmapped_unmerged)- unmapped_merged_input))+"\n")

    return merged_success

def check_merged(dir):
    merged_success = True
    met = get_glob(dir + "/metadata.json")
    f = open(met,"r")
    pObj = json.loads(f.read())
    f.close()

    #getting unmapped reads
    for output in pObj['outputs']:
        if output['label'] == "metagenome_alignment":
            unmapped = output['metadata']['num_input_reads'] - output['metadata']['num_aligned_reads'] 

    #getting "metagenome_unmapped_merged"
    for output in pObj['outputs']:
        if output['label'] == "metagenome_unmapped_merged":
            unmapped_merged_input =  output['metadata']['readcount_input'] 
            unmapped_merged = output['metadata']['readcount']

    #getting "metagenome_unmapped_unmerged"
    for output in pObj['outputs']:
        if output['label'] == "metagenome_unmapped_unmerged":
            unmapped_unmerged_input =  output['metadata']['readcount_input'] 
            unmapped_unmerged = output['metadata']['readcount']

    
    #input read count same for merged and unmerged

    if (unmapped_unmerged_input != unmapped_merged_input):
        merged_success = False
    #input read count > 20% of unaligned reads
    if (unmapped_merged_input/unmapped < 0.2):
        pass
        merged_success = False
    #unmerged and merged add up to input
    if (((unmapped_merged + unmapped_unmerged)- unmapped_merged_input) !=0):
        merged_success = False

    if merged_success == False:
        sys.stdout.write(dir + "\n")
        sys.stdout.write("unmapped " + str(unmapped)+"\n")
        sys.stdout.write("unmapped unmerged input " + str(unmapped_unmerged_input)+"\n")
        sys.stdout.write("unmapped merged input " + str(unmapped_merged_input)+"\n")
        sys.stdout.write("unmapped merged input / unmapped ratio " + str(unmapped_merged_input/unmapped)+"\n")
        sys.stdout.write("unmapped merged " + str(unmapped_merged)+"\n")
        sys.stdout.write("unmapped unmerged " + str(unmapped_unmerged)+"\n") 
        sys.stdout.write("unmapped output - input " + str(((unmapped_merged + unmapped_unmerged)- unmapped_merged_input))+"\n")

    return merged_success

    
def email_pdf(dir,email):
    email_success = True
    #uuencode *.pdf test.pdf | mail bfoster@lbl.gov -s test
    cmd = "(echo " + dir + ";uuencode " + dir + "/*.pdf test.pdf ) | mail " + email + " -s \"Metagenome Sanity Check\"" 
#    print "\n" + cmd + "\n"

    ## call command ##
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    ## Talk with date command i.e. read data from stdout and stderr. Store this info in tuple ##
    (output, err) = p.communicate()
    ## Wait for date to terminate. Get return returncode ##
    p_status = p.wait()

    if (p_status == 0):
        pass
    else:
        sys.stdout.write("Subprocess failed for " + cmd)
        email_success = False
    return email_success
    

def first_release(dir):
    first_release = True
    #get mapping readcount from metadata.json
    met = get_glob(dir + "/metadata.json")
    f = open(met,"r")
    pObj = json.loads(f.read())
    f.close()
#    return first_release
    spids = []
    spids = pObj['metadata']['sequencing_project_id']
    fail = []
    for spid in spids:
        data={"file_type":"scaffolds","metadata.sequencing_project_id":str(spid)}
        result = jgi_mgaUtils.web_services("jamo",data)
        if len(result) > 0:
            first_release=False
        
    # module load jamo; jamo info all custom "metadata.sequencing_project_id=10238406" | wc -l 
#        cmd = "module load jamo; jamo report select file_name where metadata.sequencing_project_id=" + str(spid) + " and file_type=\"scaffolds\"  |  wc -l"
#        cmd = "module load jamo; jamo info all custom \"metadata.sequencing_project_id=" + str(spid) + "\" | grep AUTO  | wc -l" 
    #    print "\n" + cmd + "\n"

#    ## call command ##
#        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
#    ## Talk with date command i.e. read data from stdout and stderr. Store this info in tuple ##
#        (output, err) = p.communicate()
#    ## Wait for date to terminate. Get return returncode ##
#        p_status = p.wait()
#
#        if (p_status == 0):
#            for line in output.split("\n"):
#                if line:
#                    if int(line) > 0:
#                        first_release = False
#                        fail.append(spid)
#        else:
#            sys.stdout.write("Subprocess failed for " + cmd)
#
#    if fail:
#        sys.stdout.write(json.dumps(fail))
    return first_release

def new_ap_id(dir):
    new_ap = True
    met = get_glob(dir + "/metadata.json")
    f = open(met,"r")
    pObj = json.loads(f.read())
    f.close()


    apids = []
    apids = pObj['metadata']['analysis_project_id']
    fail = []
    for apid in apids:
        data={"metadata.analysis_project_id":str(apid),'file_type':'scaffolds'}
        result = jgi_mgaUtils.web_services("jamo",data)
        if len(result) > 0:
            new_ap = False
        #module load jamo; jamo info all custom metadata.analysis_project_id=1059143  | wc -l 
#        cmd = "module load jamo; jamo report select file_name where metadata.analysis_project_id=" + str(apid) + " and file_type=\"scaffolds\"  |  wc -l"
#        cmd = "module load jamo; jamo info all custom \"metadata.analysis_project_id=" + str(apid) + "\"  | wc -l"
#        print "\n" + cmd + "\n"
#
#    ## call command ##
#        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
#    ## Talk with date command i.e. read data from stdout and stderr. Store this info in tuple ##
#        (output, err) = p.communicate()
#    ## Wait for date to terminate. Get return returncode ##
#        p_status = p.wait()
#
#        if (p_status == 0):
#            for line in output.split("\n"):
#                if line:
#                    if int(line) > 0:
#                        new_ap = False
#                        fail.append(apid)
#        else:
#            sys.stdout.write("Subprocess failed for " + cmd)
#
#    if fail:
#        sys.stdout.write(json.dumps(fail))
    return new_ap
    

def check_release_files(dir):
    gzip_threshold = 1000000
    non_gzip_threshold = 1024
    check_release = True

    # find . -maxdepth 1 -type l -name "*.gz" | xargs -i realpath {} | xargs -i ls -l {}  | cut -f 5 -d " " 
    cmd = "find " + dir + " -maxdepth 1 -type l -name \"*.gz\" | xargs -i realpath {} | xargs -i ls -l {}  | cut -f 5 -d \" \"" 
#    print "\n" + cmd + "\n"

    ## call command ##
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    ## Talk with date command i.e. read data from stdout and stderr. Store this info in tuple ##
    (output, err) = p.communicate()
    ## Wait for date to terminate. Get return returncode ##
    p_status = p.wait()

    if (p_status == 0):
        for line in output.split("\n"):
            if line:
                if int(line) < gzip_threshold:
                    check_release = False
                    
    else:
        sys.stdout.write("Subprocess failed for " + cmd)

    cmd = "find " + dir + " -maxdepth 1 -type l  | grep -v \"\.gz\" |  xargs -i realpath {} | xargs -i ls -l {}  | cut -f 5 -d \" \"" 
#    print "\n" + cmd + "\n"

    ## call command ##
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    ## Talk with date command i.e. read data from stdout and stderr. Store this info in tuple ##
    (output, err) = p.communicate()
    ## Wait for date to terminate. Get return returncode ##
    p_status = p.wait()

    if (p_status == 0):
        for line in output.split("\n"):
            if line:
                if int(line) < non_gzip_threshold:
                    check_release = False
    else:
        sys.stdout.write("Subprocess failed for " + cmd)

    return check_release
    
    
def check_mapping(dir):
    mapping_check = True
    #get mapping readcount from metadata.json
    met = get_glob(dir + "/metadata.json")
    f = open(met,"r")
    pObj = json.loads(f.read())
    f.close()

    for output in pObj['outputs']:
        if output['label'] == "metagenome_alignment":
            mapping_readcount = output['metadata']['num_input_reads']

    #get assy log files
    assy_logs = dir + "/pre/out.p.fq"
    globs = glob.glob(assy_logs)
    if (len(globs)<1):
        sys.stdout.write("No megahit logs " + assy_logs)
    cmd = "wc -l " + assy_logs + " | perl -ane \"printf(\\$F[0]/4)\""

    ## call command ##
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    ## Talk with date command i.e. read data from stdout and stderr. Store this info in tuple ##
    (output, err) = p.communicate()
    ## Wait for date to terminate. Get return returncode ##
    p_status = p.wait()

    if (p_status == 0):
        reads_log = output
    else:
        sys.stdout.write("Subprocess failed for " + cmd)

#    mapping_readcount.rstrip('\n')
    reads_log = int(reads_log.rstrip("\n"))
    if reads_log != mapping_readcount:
        print "from log"
        print reads_log
        print "from json"
        print mapping_readcount
        mapping_check = False

    return mapping_check
    
            
def get_glob(glob_txt):
    globs = glob.glob(glob_txt)
    if len(globs) != 1:
        sys.stdout.write("No or multiple" + glob_txt)
    return globs[0]

def metadata_json_check(metadata_file):
    f = open(metadata_file,"r")
    pObj = json.loads(f.read())
    f.close()
    empty = []
    for i in range(0,len(pObj["outputs"])):
        for key in pObj["outputs"][i].keys():
            if key == "metadata":
#            print str(key) + str(pObj["outputs"][i][key])
                for key2 in pObj["outputs"][i]["metadata"]:
                    if pObj["outputs"][i]["metadata"][key2] == "":
                        empty.append(key2)

    if len(empty) > 0:
        sys.stdout.write(str(empty))
        return False
    else:
        return True
    
def contig_count(agp,fasta,cov):
    contig_count_success = True
    #agp
    agp_contigs=dict()
    agp_scaffolds=dict()
    with open(agp,"r") as f:
        for line in f:
            if line.startswith("#"):
                pass
            else:
                fields = line.split()
                agp_contigs[fields[5]]=1
                agp_scaffolds[fields[0]]=1

    #fasta
    fasta_scaffolds=dict()
    with open(fasta,"r") as f:
        for line in f:
            if line.startswith(">"):
                header = re.sub('^>', '' , line)
                header = header.rstrip("\n")            
                fasta_scaffolds[header]=1

    #cov
    cov_scaffolds=dict()
    with open(cov,"r") as f:
        for line in f:
            if line.startswith("ID"):
                pass
            elif line.startswith("#"):
                print "# in header"
                contig_count_successs = False
            else:
                fields = line.split()
                cov_scaffolds[fields[0]]=1

    if (has_missing_keys(agp_scaffolds,fasta_scaffolds)):
        contig_count_success = False
        sys.stdout.write("agp,fasta,scaffolds")
    if (has_missing_keys(agp_scaffolds,cov_scaffolds)):
        contig_count_success = False
        sys.stdout.write("agp,cov,scaffolds")

    return contig_count_success

def has_missing_keys(a,b):
    has_missing = False
    if (len(set(a.keys()) - set(b.keys())) !=0 or  len(set(b.keys()) - set(a.keys())) !=0):
        has_missing=True
    return has_missing
    
            
if __name__ == "__main__":
        main()

