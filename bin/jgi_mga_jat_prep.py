#!/usr/bin/env python
'''
jgi_mga_jat_prep.py
takes a metadata.json pre file, copies the files to targets
'''

import argparse
import sys
import os
import json
import shutil

VERSION = "0.0.1"

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " +  VERSION + ")")
    parser.add_argument("-m", "--metadata", required=True, help="metadata.json file  (or metadata.json.pre file from unprep)\n")
    parser.add_argument("-u", "--unprep", required=False, action='store_true', default=False, help="unprep\n")

    args = parser.parse_args()
    filename = args.metadata
    if not os.path.exists(filename):
        print filename + " not found"
        sys.exit()

    file_dir = os.path.dirname(os.path.realpath(filename)) + "/"
    if args.unprep and os.path.splitext(filename)[1] != ".pre":
        print  os.path.splitext(filename)[1]
        print filename + " must be a backed up .pre file from jgi_mgaJATprep.py to unprep"
        sys.exit()

    with open(filename, "r") as readfile:
        metadata = json.loads(readfile.read())

    tomv = []
    for i in range(0, len(metadata["outputs"])):
        if not ":" in metadata["outputs"][i]["file"]:
            print filename + " doesn't have source and dest in file field"
            sys.exit()

        (source, dest) = metadata["outputs"][i]["file"].split(":")
        if args.unprep:
            tomv.append(dest + ":" + source)
        else:
            tomv.append(source + ":" + dest)
            metadata["outputs"][i]["file"] = os.path.basename(dest)



    for cmd in tomv:
        (source, dest) = cmd.split(":")
        #print "shutil.move(source, dest):" + source + " " + dest
        shutil.move(source, dest)

    if args.unprep:
        shutil.move(filename, os.path.splitext(filename)[0])
        if os.path.exists(file_dir + "rqc-files.txt"):
            shutil.move(file_dir + "rqc-files.txt", file_dir + "rqc-files.txt.tmp")
        if os.path.exists(file_dir + "rqc-stats.txt"):
            shutil.move(file_dir + "rqc-stats.txt", file_dir + "rqc-stats.txt.tmp")
        if os.path.exists(file_dir + "status.log"):
            shutil.move(file_dir + "status.log", file_dir + "status.log.tmp")

    else:
        shutil.copy(filename, filename + ".pre")
        with open(filename, "w") as writefile:
            writefile.write(json.dumps(metadata, indent=3))

        if os.path.exists(file_dir + "rqc-files.txt.tmp"):
            shutil.move(file_dir + "rqc-files.txt.tmp", file_dir + "rqc-files.txt")
        if os.path.exists(file_dir + "rqc-stats.txt.tmp"):
            shutil.move(file_dir + "rqc-stats.txt.tmp", file_dir + "rqc-stats.txt")
        if os.path.exists(file_dir + "status.log.tmp"):
            shutil.move(file_dir + "status.log.tmp", file_dir + "status.log")
    return

if __name__ == "__main__":
    main()
