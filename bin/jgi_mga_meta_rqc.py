#!/usr/bin/env python

import argparse
import sys
import os
from datetime import datetime
import json
import time
import subprocess

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__))+'/../lib')
import jgi_mga_utils

"""
metagenome RQC assembly, reporting wrapper
calls 
1) jgi_mga_info_json.py to gather inputs and reporting metadata
2) jgi_mga_create_pipeline_yaml.py to create template
3) pipeline_runner.py to actually run the pipeline


Dependencies:
jgi_mga_utils.py 
jgi_mga_info_json.py
jgi_mga_create_pipeline_yaml.py
pipeline_runner.py

v 2.0: 20170719
- initial version of updated pipeline 

v 2.1.0: 20171026
- added sigs table, rqc pdf report, changed input to alignment to be filtered,
  

"""


VERSION = "2.1.0"

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Metagenome rqc pipeline.'
                                     , usage="%(prog)s [options]")

    parser.add_argument('-v', '--version', action='version', 
                        version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-o", "--output-path", required=True, 
                        help="output path to place intermediate and final files.\n")

    parser.add_argument("-s", "--spid", required=False, 
                        help="metagenome project id.\n")
    parser.add_argument("-a", "--apid", required=False, 
                        help="metagenome analysis project id.\n")
    parser.add_argument("-l", "--lib", required=False, 
                        help="only include from this library id (must be used with spid or apid).\n")

    parser.add_argument("-t", "--template", required=True, 
                        help="name of template to be used.\n")
    parser.add_argument("-f", "--force_complete", default=False, required=False, action='store_true', 
                        help="try to force complete.\n")
    parser.add_argument("-d", "--debug", default=False, action='store_true', 
                        help="[Optional] Setup but do not run\n")

    args = parser.parse_args()

    output_id = ""
    if not args.spid and not args.apid and not args.lib:
        sys.exit("No project id, analysys id, or library id project specified")
    elif args.lib and (not args.spid and not args.apid):
        sys.exit("library requires spid or apid")
    elif args.spid and args.apid:
        sys.exit("Can't specify both project or analisys project specified")
    elif args.spid:
        output_id = args.spid
    else:
        output_id = args.apid



    if not os.path.exists(args.output_path):
        os.mkdir(args.output_path)
    elif not os.access(args.output_path, os.W_OK):
        sys.exit("output directory " + args.output_path + " is not writable")

    bindir = os.path.abspath(os.path.dirname(sys.argv[0]))
    
    infojsonfile = args.output_path + "/" + output_id + ".json"
    #######################create info file
    create_info = bindir + "/jgi_mga_info_json.py"

    if args.force_complete:
        cmd = [create_info, "-o", infojsonfile, "--force_complete"]
    else:
        cmd = [create_info, "-o", infojsonfile]
    if args.spid:
        cmd.append("-s")
        cmd.append(str(output_id))
    else:
        cmd.append("-a")
        cmd.append(str(output_id))

    ###################################
    # jgi_mgaInfoJson gets project input file information and reporting  metadata 
    jgi_mga_utils.run_sub(" ".join(cmd), 
                         args.output_path + "/jgi_mga_info_json.py", args.debug)

    with open(infojsonfile, "r") as readfile:
        info = json.loads(readfile.read())

    if args.lib:
        for i in range(0,len(info['sequencing_projects'])):
            keep_list = list()
            for j in range(0,len(info['sequencing_projects'][i]['lib'])):
                if info['sequencing_projects'][i]['lib'][j]['lib'] == args.lib:
                    keep_list.append(info['sequencing_projects'][i]['lib'][j])
            info['sequencing_projects'][i]['lib']=keep_list
        with open(infojsonfile, "w") as writefile:                    
            writefile.write(json.dumps(info,indent=3))
            

    check_filtered_input_files(info)# and fetch

    template = ""
    if args.template:
        code_dir_template = os.path.dirname(os.path.realpath(__file__)) + "/" + os.path.basename(args.template)
        if args.template.find('/') != -1:
            template = args.template
        elif os.path.exists(code_dir_template):
            template = code_dir_template
        else:
            sys.exit("template does not exist in " + os.path.dirname(os.path.realpath(__file__))) + " check the template name and/or specify full path"
    else:
        sys.exit("template required for now ... auto template coming soon")
        template = determine_template(info)

    #validate for full path then relative path template
    if os.path.exists(template):
        pass
    elif os.path.exists(bindir + "/" + template):
        template = bindir + "/" + template
    else:
        sys.exit("can't find template: " + template)

    #### populate template data with input and environment data ####
    create_pipeline_exe = bindir + "/jgi_mga_create_pipeline_yaml.py"
    cmd = [create_pipeline_exe, "-x", "-i", infojsonfile]
    cmd.append("-t")
    cmd.append(template)
    jgi_mga_utils.run_sub(" ".join(cmd), args.output_path + "/jgi_mga_create_pipeline_yaml.py", args.debug)

    #### create bash file to run pipeline using pipelinRunner ####
    config_file = infojsonfile+ ".config.yaml"
    bash_file = infojsonfile+ ".config.yaml.bash"
    with open(bash_file, "w") as write_file:
        write_file.write("#!/usr/bin/env bash\n\n")
        write_file.write("cd " + args.output_path + "\n\n")
        runner_cmd = bindir + "/pipeline_runner.py -f "  + config_file 
        runner_cmd += " 1>> " + config_file + ".o "
        runner_cmd += " 2>| " + config_file + ".e"
        write_file.write(runner_cmd + "\n")

    logfile = config_file + ".o"

    #run pipeline
    if args.debug == False:
        start_time = "Start, " + jgi_mga_utils.gettimestamp() + "\n"
        with open(logfile, "a") as write_file:
            write_file.write(start_time)
        jgi_mga_utils.run_sub("bash " + bash_file, bash_file, args.debug)
    else:
        sys.exit()

    #check pipeline output for errors  ####### verify this
    #get last line of file
    with open(logfile, "r") as read_file:
        lastline = list(read_file)[-1]

    if lastline.find("success") != -1:
        pass
    elif lastline.find("error") != -1:
        end_time = "failed, " + jgi_mga_utils.gettimestamp() + "\n"
        with open(logfile, "a") as append_file:
            append_file.write(end_time)
        sys.exit(1)
    else:
        sys.exit("check " + logfile + " for errors")


    end_time = "complete, " + jgi_mga_utils.gettimestamp() + "\n"
    with open(logfile, "a") as append_file:
        append_file.write(end_time)

#    task_id = info["analysis_task_id"]
#    try:
#        qcutils.analysis_task_status(task_id, 8) #"In Progress"
#    except:
#        print sys.stderr.write("failed to change status " + str(task_id) + " to " + str(8) + "\n")


def check_filtered_input_files(info_json, fetch=True):
    '''read input files where usable !=false, fetch off jamo if possible'''
    for i in range(len(info_json['sequencing_projects'])):
        for j in range(len(info_json['sequencing_projects'][i]['lib'])):
            if info_json['sequencing_projects'][i]['lib'][j]['usable'] != False:
                if not os.path.exists(info_json['sequencing_projects'][i]['lib'][j]['filtered']):
                    file_ = os.path.basename(info_json['sequencing_projects'][i]['lib'][j]['filtered'])
                    jamo_result = jgi_mga_utils.web_services("jamo", {"file":file_})
                    if len(jamo_result) == 1  and jamo_result[0].find("PURGED") != -1 and fetch == True:
                        sys.exit(file_ + " is purged, please recover")
                        result = jamo_fetch_and_wait(file_)
                        if result == "MULTIPLE_FILES":
                            sys.exit("Mutliple files detected")
                        elif result == "":
                            sys.exit(file_ + " is not recovered")
                        else:
                            pass
                    else:
                        sys.exit(file_ + " does not exist in jamo or is in jamo multiple times\n")

def jamo_fetch_and_wait(filename, timeout=30):
    '''input: file, output: file or "MULTIPLE_FILES"'''
    return ""
    ############# Update to use webservice ###########
    cmd = "jamo fetch all filename " + filename
    timecount = 0
    return_val = ""
    while True:
        if timecount >= timeout:
            break
        lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
        jamo_lines = "".join(lines)

        if len(lines) == 1:
            if jamo_lines.find("PURGED") != -1 or jamo_lines.find("RESTORE_IN_PROGRESS") != -1:
                pass
            else:
                return_val = filename
                break
        else:
            return_val = "MULTIPLE_FILES"
            break
        time.sleep(60)
        timecount += 1
    return return_val

def determine_template(info_json):
    '''guess at template to use based on input. Not yet implemented'''
    #TODO implement determine_template
    template = ""
#    input_count = 0
#    filtered_read_count = 0
    #add rules for cori vs genepool and eventually memory prediction
    return template



if __name__ == "__main__":
    main()
