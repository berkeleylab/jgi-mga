#!/usr/bin/env python

import sys, os
import glob
import re
import json
import shutil

if len(sys.argv)==1 or not os.path.exists(sys.argv[1]):
    sys.exit("Usage " + sys.argv[0] + ": dir")

def main():
    dir = sys.argv[1]
    dir = dir.rstrip('/')
    assem_metadata = glob.glob(dir + '/*Assembly/metadata.json')
    align_metadata = glob.glob(dir + '/*ontigs/metadata.json')

    if len(assem_metadata)!=1 or len(assem_metadata)!=1:
        sys.exit("metadata.jsons not correct")
    dir_text = [".mtAssembly",".final.contigs"]
    for glob_dir in dir_text:
        print alter_metadata_json(dir, glob_dir)

def alter_metadata_json(dir, glob_txt):
    metadata = glob.glob(dir + '/*' + glob_txt + '/metadata.json')
    if len(metadata)!=1:
        sys.exit("no metadata for " + glob_txt)
    else:
        metadata = metadata[0]
    with open(metadata, "r") as infile:
        md=json.loads(infile.read())
    for i in range(0,len(md['outputs'])):
        if md['outputs'][i]['label']=='shell_script':
            md['outputs'][i]['file'] = dir + '/' + os.path.basename(md['outputs'][i]['file'])
            continue
        regex1 = r'^.*/(.*?'+ glob_txt + '/)'
        regex2 = dir + "/"
        md['outputs'][i]['file'] = re.sub(regex1, regex2 + r'\1' , md['outputs'][i]['file'])
    shutil.move(metadata, metadata + ".bak")
    with open(metadata, "w") as outfile:
        outfile.write(json.dumps(md,indent=3))
    return metadata
              

if __name__ == "__main__":
    main()
